﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day12
{
    internal class Solver
    {
        public long First(IEnumerable<string> input)
        {
            var pots = ParseInput(input);
            pots.SimulateGenerations(20);

            return pots.PlantIndexSum;
        }

        public long Second(IEnumerable<string> input)
        {
            var pots = ParseInput(input);
            pots.SimulateGenerations(50000000000L);

            return pots.PlantIndexSum;
        }

        private Simulator ParseInput(IEnumerable<string> input)
        {
            var parser = new Parser();

            return parser.GetSimulator(input);
        }
    }
}
