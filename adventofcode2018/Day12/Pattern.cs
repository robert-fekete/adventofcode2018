﻿using System;

namespace adventofcode2018.Day12
{
    internal class Pattern
    {
        private bool[] state;

        public Pattern(bool[] state, bool result)
        {
            this.state = state;
            Result = result;
        }

        public int Length => state.Length;

        public bool Result { get; internal set; }

        public bool IsMatch(long middle, Func<long, bool> isPlant)
        {
            var offset = middle - (state.Length - 1) / 2;
            for(int i = 0; i < state.Length; i++)
            {
                if (state[i] != isPlant(offset + i))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
