﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day12
{
    internal class Parser
    {
        public Simulator GetSimulator(IEnumerable<string> input)
        {
            var pots = GetPots(input.First());

            var patterns = new List<Pattern>();
            foreach(var line in input.Skip(2))
            {
                var parts = line.Split('>');
                var state = parts[0].Trim(' ', '=').Select(c => c == '#').ToArray();
                var result = parts[1].Trim(' ')[0] == '#';

                var pattern = new Pattern(state, result);
                patterns.Add(pattern);
            }

            return new Simulator(pots, patterns);
        }

        private PotCollection GetPots(string line)
        {
            var parts = line.Split(':');

            var initialState = parts[1].Trim().Select(c => c == '#').ToArray();

            return new PotCollection(initialState);
        }
    }
}
