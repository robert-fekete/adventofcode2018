﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day12
{
    internal class PotCollection
    {
        private const bool DEFAULT_VALUE = false;
        private readonly Dictionary<long, bool> pots = new Dictionary<long, bool>();

        public PotCollection()
        { }

        public PotCollection(bool[] initialState)
            : this()
        {
            for(int i = 0; i < initialState.Length; i++)
            {
                pots[i] = initialState[i];
            }
        }

        public long MinIndex => GetPlantIndices().Min();

        public long MaxIndex => GetPlantIndices().Max();

        public long PlantSum => GetPlantIndices().Sum();

        public bool this[long key]
        {
            get
            {
                if (!pots.ContainsKey(key))
                {
                    return DEFAULT_VALUE;
                }
                else
                {
                    return pots[key];
                }
            }
            set
            {
                pots[key] = value;
            }
        }

        internal bool IsSamePattern(PotCollection other)
        {
            var start = MinIndex;
            var end = MaxIndex;

            var otherStart = other.MinIndex;
            var otherEnd = other.MaxIndex;

            if (end - start != otherEnd - otherStart)
            {
                return false;
            }

            var offset = otherStart - start;
            for(long i = start; i <= end; i++)
            {
                var otherIndex = i + offset;
                if (this[i] != other[otherIndex])
                {
                    return false;
                }
            }

            return true;
        }

        internal void Shift(long offset)
        {
            foreach(var key in pots.Keys.OrderByDescending(k => k))
            {
                pots[key + offset] = pots[key];
                pots[key] = false;
            }
        }

        private IEnumerable<long> GetPlantIndices()
        {
            return pots.Where(kvp => kvp.Value).Select(kvp => kvp.Key);
        }

        internal void Print(int start, int end)
        {
            for (int i = start; i <= end; i++)
            {
                Console.Write(this[i] ? '#' : '.');
            }
            Console.WriteLine();
        }
    }
}
