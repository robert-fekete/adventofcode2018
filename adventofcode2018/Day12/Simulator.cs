﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day12
{
    internal class Simulator
    {
        private List<Pattern> patterns;
        private PotCollection pots;
        private readonly int offset;

        public Simulator(PotCollection pots, List<Pattern> patterns)
        {
            this.pots = pots;
            this.patterns = patterns;
            offset = (patterns.First().Length - 1) / 2;
        }

        public long PlantIndexSum => pots.PlantSum;

        internal void SimulateGenerations(long numberOfGenerations)
        {
            for(long i = 0; i < numberOfGenerations; i++)
            {
                var tempPots = pots;
                SimulateGeneration();

                if (tempPots.IsSamePattern(pots))
                {
                    var offset = pots.MinIndex - tempPots.MinIndex;
                    var generationsLeft = numberOfGenerations - (i + 1);
                    pots.Shift(offset * generationsLeft);
                    return;
                }
            }
        }
        
        private void SimulateGeneration()
        {
            var nextPots = new PotCollection();
            var start = pots.MinIndex - offset;
            var end = pots.MaxIndex + offset;
            for(long i = start; i < end; i++)
            {
                var pattern = patterns.FirstOrDefault(p => p.IsMatch(i, index => pots[index]));
                if (pattern == null)
                {
                    continue;
                }
                var isPlant = pattern.Result;

                nextPots[i] = isPlant;
            }
            //nextPots.Print(-2, 190);

            pots = nextPots;
        }
    }
}
