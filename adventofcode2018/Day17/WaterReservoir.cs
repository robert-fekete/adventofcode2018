﻿using System.Collections.Generic;

namespace adventofcode2018.Day17
{
    internal class WaterReservoir : IObstacle
    {
        private readonly HashSet<Position> water = new HashSet<Position>();

        public int TotalVolume => water.Count;

        public bool HasPosition(Position position)
        {
            return water.Contains(position);
        }

        internal void Add(Water w)
        {
            water.Add(w.Current);
        }

        internal void Remove(Water w)
        {
            if (water.Contains(w.Current))
            {
                water.Remove(w.Current);
            }
        }
    }
}
