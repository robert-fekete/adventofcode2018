﻿using System;
using System.Diagnostics;

namespace adventofcode2018.Day17
{
    [DebuggerDisplay("({X}:{Y})")]
    internal class Position
    {
        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }

        internal Position AddY(int offset)
        {
            return new Position(X, Y + offset);
        }

        internal Position AddX(int offset)
        {
            return new Position(X + offset, Y);
        }

        public bool IsHorisontallyAligned(Position other)
        {
            return X == other.X;
        }

        public bool IsInVerticalScope(Range scope)
        {
            return scope.InRange(Y);
        }

        public override bool Equals(object obj)
        {
            return obj is Position position &&
                   X == position.X &&
                   Y == position.Y;
        }

        public override int GetHashCode()
        {
            var hashCode = 1502939027;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return $"({X}:{Y})";
        }
    }
}
