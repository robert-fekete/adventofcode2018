﻿namespace adventofcode2018.Day17
{
    internal class Range
    {
        private readonly int min;
        private readonly int max;

        public Range(int min, int max)
        {
            this.min = min;
            this.max = max;
        }

        internal bool InRange(int value)
        {
            return min <= value && value <= max;
        }
    }
}
