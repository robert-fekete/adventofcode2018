﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day17
{
    internal class Simulator
    {
        private readonly bool verbose = false;
        private readonly bool createBitmap = false;

        private readonly WaterReservoir settled = new WaterReservoir();
        private readonly WaterReservoir flowing = new WaterReservoir();
        private readonly Stack<Water> backlog = new Stack<Water>();
        private readonly Cave cave;

        private readonly IObstacle obstacles;

        public Simulator(Cave cave, Water spring)
        {
            this.cave = cave;
            backlog.Push(spring);

            obstacles = new CompositeObstacle(cave, settled);
        }

        public (WaterReservoir, WaterReservoir) Run()
        {
            var range = cave.GetVerticalRange();
            RunInitial(range);

            while (backlog.Any())
            {
                var current = backlog.Pop();

                var fallingInInfinity = DoWaterFallDown(range, current);
                if (fallingInInfinity)
                {
                    continue;
                }

                Print();

                // This is needed due to the weird logic in DoWaterFallDown:
                // There is a twisted logic in that function. It checks if 
                // the water can fall to the next cell, but inside the loop
                // doesn't "make the next one flow", but insted the current 
                // one. So here we need the next one (now current one) flow
                // I tried to make the logic more straight forward, but couldn't
                // really figure out, some flowing.Add or backlog.Add was 
                // always missing. Maybe future me... 
                flowing.Add(new Water(current));

                DoWaterSideFlow(current);
            }

            Print(isBitmap: true);
            return (settled, flowing);
        }

        private bool DoWaterFallDown(Range range, Water current)
        {
            while (CanFallDown(current) && !AlreadyFlowing(current.NextDown()))
            {
                var toKeep = new Water(current);
                backlog.Push(toKeep);
                flowing.Add(toKeep);

                current.MoveDown();

                if (FallingInInfinity(range, current))
                {
                    PurgeWaterFallFromBacklog(current);
                    return true;
                }
            }

            return false;
        }

        private static bool FallingInInfinity(Range range, Water current)
        {
            return !current.IsInScope(range);
        }

        private bool AlreadyFlowing(Position next)
        {
            return flowing.HasPosition(next);
        }

        private bool CanFallDown(Water current)
        {
            return !obstacles.HasPosition(current.NextDown());
        }

        private void DoWaterSideFlow(Water current)
        {
            if (obstacles.HasPosition(current.NextDown()))
            {
                var temp = new List<Water>();
                temp.Add(current);

                var wallOnLeft = GoLeft(new Water(current), temp);
                var wallOnRight = GoRight(new Water(current), temp);

                var isWaterSettled = wallOnLeft && wallOnRight;
                if (isWaterSettled)
                {
                    FillTempToSettledReservoir(temp);
                }
                else
                {
                    FillTempToFlowingReservoir(temp);
                }

                Print();
            }
        }

        private void FillTempToSettledReservoir(List<Water> temp)
        {
            foreach (var water in temp)
            {
                flowing.Remove(water);
                settled.Add(water);
            }
        }

        private void FillTempToFlowingReservoir(List<Water> temp)
        {
            foreach (var water in temp)
            {
                flowing.Add(water);
            }
        }

        private void PurgeWaterFallFromBacklog(Water current)
        {
            while (backlog.Peek().SameWaterFall(current))
            {
                backlog.Pop();
            }
        }

        private bool GoRight(Water current, List<Water> temp)
        {
            return Go(current, temp, w => w.NextRight(), w => w.MoveRight());
        }

        private bool GoLeft(Water current, List<Water> temp)
        {
            return Go(current, temp, w => w.NextLeft(), w => w.MoveLeft());
        }

        private bool Go(Water current, List<Water> temp, Func<Water, Position> getNext, Action<Water> move)
        {
            while (!obstacles.HasPosition(getNext(current)))
            {
                move(current);
                temp.Add(new Water(current));
                if (!obstacles.HasPosition(current.NextDown()))
                {
                    backlog.Push(new Water(current));
                    current.MoveDown();
                    backlog.Push(current);

                    return false;
                }
            }

            return true;
        }

        private void RunInitial(Range vertical)
        {
            var water = backlog.Pop();
            while (!water.IsInScope(vertical))
            {
                water.MoveDown();
            }

            backlog.Push(water);
        }

        private void Print(bool isBitmap = false)
        {
            if (verbose || (isBitmap && createBitmap))
            {
                cave.Print(settled, flowing, isBitmap);
                Console.WriteLine();
            }
        }
    }
}
