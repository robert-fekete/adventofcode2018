﻿using System.Linq;

namespace adventofcode2018.Day17
{
    internal class CompositeObstacle : IObstacle
    {
        private readonly IObstacle[] obstacles;
        
        public CompositeObstacle(params IObstacle[] obstacles)
        {
            this.obstacles = obstacles;
        }

        public bool HasPosition(Position position)
        {
            return obstacles.Any(o => o.HasPosition(position));
        }
    }
}