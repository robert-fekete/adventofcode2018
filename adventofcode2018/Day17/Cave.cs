﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace adventofcode2018.Day17
{
    // TODO Cave could be less coupled to Position
    internal class Cave : IObstacle
    {
        private HashSet<Position> positions;

        private Cave(IEnumerable<Position> positions)
        {
            this.positions = new HashSet<Position>(positions);
        }

        public Range GetVerticalRange()
        {
            var minY = positions.Min(p => p.Y);
            var maxY = positions.Max(p => p.Y);

            return new Range(minY, maxY);
        }

        public bool HasPosition(Position position)
        {
            return positions.Contains(position);
        }

        public static Cave FromInput(IEnumerable<string> input)
        {
            var positions = new List<Position>();
            foreach (var line in input)
            {
                var parts = line.Split(' ');
                (var tag1, var values1) = ParseValues(parts[0]);
                (var tag2, var values2) = ParseValues(parts[1]);

                var xValues = tag1 == "x" ? values1 : values2;
                var yValues = tag1 == "x" ? values2 : values1;

                foreach (var x in xValues)
                {
                    foreach (var y in yValues)
                    {
                        positions.Add(new Position(x, y));
                    }
                }
            }

            return new Cave(positions);
        }

        private static (string, IEnumerable<int>) ParseValues(string token)
        {
            var parts = token.Trim(',').Split('=');
            var name = parts[0];

            if (!parts[1].Contains(".."))
            {
                return (name, new[] { int.Parse(parts[1]) });
            }

            var ends = parts[1].Split('.');
            var start = int.Parse(ends[0]);
            var end = int.Parse(ends[2]);

            return (name, Enumerable.Range(start, end - start + 1));
        }

        #region Printing
        private Bitmap bitmap;
        public void Print(WaterReservoir settled, WaterReservoir flowing, bool isBitmap)
        {
            var minX = positions.Min(p => p.X);
            var maxX = positions.Max(p => p.X);
            var minY = positions.Min(p => p.Y);
            var maxY = positions.Max(p => p.Y);

            Action<int, int, char> print = PrintConsole;
            if (isBitmap)
            {
                var resolution = 2;
                bitmap = new Bitmap(resolution * (maxX - minX + 1), resolution * (maxY - minY + 1));
                print = (x, y, w) => PrintBitmap(bitmap, resolution, x, y, w);
            }

            for (int y = minY; y <= maxY; y++)
            {
                for (int x = minX; x <= maxX; x++)
                {
                    var position = new Position(x, y);
                    if (settled.HasPosition(position))
                    {
                        print(x - minX, y - minY, '~');
                    }
                    else if (flowing.HasPosition(position))
                    {
                        print(x - minX, y - minY, '|');
                    }
                    else if (positions.Contains(position))
                    {
                        print(x - minX, y - minY, '#');
                    }
                    else
                    {
                        print(x - minX, y - minY, '.');
                    }
                }
                if (!isBitmap)
                {
                    Console.WriteLine();
                }
            }
            if (isBitmap)
            {
                bitmap.Save(Path.Combine(@"C:\Users\Roberto\source\repos\adventofcode2018\adventofcode2018", "Day17_2.jpg"));
            }
        }

        private void PrintConsole(int _1, int _2, char what)
        {
            Console.Write(what);
        }

        private void PrintBitmap(Bitmap bitmap, int resolution, int x, int y, char what)
        {
            switch (what)
            {
                case '~':
                    SetPixel(bitmap, x, y, resolution, Color.Blue);
                    break;
                case '|':
                    SetPixel(bitmap, x, y, resolution, Color.Cyan);
                    break;
                case '#':
                    SetPixel(bitmap, x, y, resolution, Color.Brown);
                    break;
                case '.':
                    SetPixel(bitmap, x, y, resolution, Color.Beige);
                    break;
            }
        }

        private void SetPixel(Bitmap bitmap, int x, int y, int resolution, Color color)
        {
            for(int xd = 0; xd < resolution; xd++)
            {
                for(int yd = 0; yd< resolution; yd++)
                {
                    bitmap.SetPixel(x * resolution + xd, y * resolution + yd, color);
                }
            }
        }
        #endregion
    }
}
