﻿namespace adventofcode2018.Day17
{
    internal interface IObstacle
    {
        bool HasPosition(Position position);
    }
}
