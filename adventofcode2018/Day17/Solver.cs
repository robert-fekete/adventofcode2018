﻿using System.Collections.Generic;

namespace adventofcode2018.Day17
{
    internal class Solver
    {
        public int First(IEnumerable<string> input)
        {
            var cave = Cave.FromInput(input);
            var simulator = new Simulator(cave, new Water(new Position(500, 0)));

            (var settled, var flowing) = simulator.Run();

            return settled.TotalVolume + flowing.TotalVolume;
        }

        internal int Second(IEnumerable<string> input)
        {
            var cave = Cave.FromInput(input);
            var simulator = new Simulator(cave, new Water(new Position(500, 0)));

            (var settled, var _) = simulator.Run();

            return settled.TotalVolume;
        }
    }
}
