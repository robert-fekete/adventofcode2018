﻿using System;
using System.Diagnostics;

namespace adventofcode2018.Day17
{
    [DebuggerDisplay("{Current}")]
    internal class Water
    {
        public Water(Position position)
        {
            Current = position;
        }

        public Water(Water other)
        {
            Current = other.Current;
        }

        public Position Current { get; private set; }

        public bool SamePosition(Position other)
        {
            return Current.Equals(other);
        }

        public bool SameWaterFall(Water other)
        {
            return Current.IsHorisontallyAligned(other.Current);
        }

        public bool IsInScope(Range vertical)
        {
            return Current.IsInVerticalScope(vertical);
        }

        public void MoveDown()
        {
            Current = NextDown();
        }

        public Position NextDown()
        {
            return Current.AddY(1);
        }

        public void MoveLeft()
        {
            Current = NextLeft();
        }

        public Position NextLeft()
        {
            return Current.AddX(-1);
        }

        public void MoveRight()
        {
            Current = NextRight();
        }

        public Position NextRight()
        {
            return Current.AddX(1);
        }
    }
}
