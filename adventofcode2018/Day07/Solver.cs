﻿using System.Collections.Generic;

namespace adventofcode2018.Day07
{
    internal class Solver
    {
        public string First(IEnumerable<string> input)
        {
            var parser = new GraphParser();
            var graph = parser.Parse(input);
            var sorter = new TopologicalSorter();
            var topologicalSorting = sorter.GetTopologicalSorting(graph);

            return topologicalSorting;
        }
        public int Second(IEnumerable<string> input, int numberOfWorkers, int baseTime)
        {
            var parser = new GraphParser();
            var graph = parser.Parse(input);
            var scheduler = new Scheduler();
            var time = scheduler.GetRunTime(graph, numberOfWorkers, baseTime);

            return time;
        }
    }
}
