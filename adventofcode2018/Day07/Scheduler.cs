﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day07
{
    internal class Scheduler
    {
        private int baseTime;
        private Graph graph;

        private SortedSet<string> pool;
        private Dictionary<string, int> finishTime;
        private int[] work;
        private int ellapsedTime;

        public int GetRunTime(Graph graph, int numberOfWorkers, int baseTime)
        {
            this.baseTime = baseTime;
            this.graph = graph;
            pool = new SortedSet<string>(graph.GetRoots());
            finishTime = new Dictionary<string, int>();
            work = new int[numberOfWorkers];
            ellapsedTime = 0;

            ScheduleWork();

            while (work.Any(w => w > 0))
            {
                StepWork();
                UpdateWorkPool();
                ScheduleWork();
            }

            return ellapsedTime;
        }

        private void ScheduleWork()
        {
            for (int i = 0; i < work.Length; i++)
            {
                if (pool.Count == 0)
                {
                    return;
                }

                if (work[i] == 0)
                {
                    var current = pool.Min;
                    pool.Remove(current);

                    var workTime = baseTime + (current[0] - 'A' + 1);
                    finishTime[current] = ellapsedTime + workTime;

                    work[i] = workTime;
                }
            }
        }

        private void StepWork()
        {
            var deltaTime = work.Where(w => w != 0).Min();
            for (int i = 0; i < work.Length; i++)
            {
                if (work[i] != 0)
                {
                    work[i] -= deltaTime;
                }
            }

            ellapsedTime += deltaTime;
        }

        private void UpdateWorkPool()
        {
            var recentlyFinished = finishTime.Where(kvp => kvp.Value == ellapsedTime)
                                    .Select(kvp => kvp.Key);

            foreach (var current in recentlyFinished)
            {
                foreach (var next in graph.GetNeighbors(current))
                {
                    if (AllUpstreamFinished(next))
                    {
                        pool.Add(next);
                    }
                }
            }
        }

        private bool AllUpstreamFinished(string node)
        {
            return graph.GetUpstream(node).All(n => finishTime.ContainsKey(n) &&
                                                    finishTime[n] <= ellapsedTime);
        }
    }
}
