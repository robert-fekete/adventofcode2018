﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day07
{
    internal class GraphParser
    {
        private const int  FROM_INDEX = 1;
        private const int TO_INDEX = 7;

        public Graph Parse(IEnumerable<string> input)
        {
            var graph = new Graph();

            foreach(var line in input)
            {
                var parts = line.Split(' ');
                var from = parts[FROM_INDEX];
                var to = parts[TO_INDEX];

                graph.AddEdge(from, to);
            }

            return graph;
        }
    }
}
