﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace adventofcode2018.Day07
{
    internal class TopologicalSorter
    {
        public string GetTopologicalSorting(Graph graph)
        {
            var pool = new SortedSet<string>(graph.GetRoots());

            var visited = new HashSet<string>();
            var result = new StringBuilder();
            while(pool.Count > 0)
            {
                var current = pool.Min;
                pool.Remove(current);

                visited.Add(current);
                result.Append(current);

                foreach(var next in graph.GetNeighbors(current))
                {
                    var upstream = graph.GetUpstream(next);
                    if (upstream.All(n => visited.Contains(n)))
                    {
                        pool.Add(next);
                    }
                }
            }

            return result.ToString();
        }
    }
}
