﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day07
{
    internal class Graph
    {
        private readonly HashSet<string> nodes = new HashSet<string>();
        private readonly Dictionary<string, List<string>> neighbors = new Dictionary<string, List<string>>();
        private readonly Dictionary<string, List<string>> upstream = new Dictionary<string, List<string>>();

        internal void AddEdge(string from, string to)
        {
            AddNode(from);
            AddNode(to);

            neighbors[from].Add(to);
            upstream[to].Add(from);
        }

        private void AddNode(string node)
        {
            nodes.Add(node);
            if (!neighbors.ContainsKey(node))
            {
                neighbors[node] = new List<string>();
            }

            if (!upstream.ContainsKey(node))
            {
                upstream[node] = new List<string>();
            }
        }

        internal IEnumerable<string> GetUpstream(string node)
        {
            if (!upstream.ContainsKey(node))
            {
                return Enumerable.Empty<string>();
            }

            return upstream[node];
        }

        internal IEnumerable<string> GetRoots()
        {
            return upstream.Where(kvp => kvp.Value.Count == 0).Select(kvp => kvp.Key);
        }

        internal IEnumerable<string> GetNodes()
        {
            return nodes;
        }

        internal IEnumerable<string> GetNeighbors(string node)
        {
            if (!neighbors.ContainsKey(node))
            {
                return Enumerable.Empty<string>();
            }

            return neighbors[node];
        }
    }
}
