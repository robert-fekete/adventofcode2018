﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day15.Graphs
{
    internal class ResultBuilder
    {
        private readonly Dictionary<Node, Node> parents = new Dictionary<Node, Node>();
        private readonly List<Node> nodes = new List<Node>();

        public bool HasResult => nodes.Any();

        internal void AddPathSegment(Node parent, Node current)
        {
            if (parent != null)
            {
                parents[current] = parent;
            }
        }
        
        internal void AddNode(Node node)
        {
            nodes.Add(node);
        }

        internal Node GetClosestNode()
        {
            if (!HasResult)
            {
                throw new InvalidOperationException("There isn't any closest node found");
            }

            nodes.Sort(new ReadingOrderComparer());

            return nodes[0];
        }

        internal Node GetFirstNodeInPathTo(Node target)
        {
            var iterNode = parents[target];
            var previousNode = target;
            while (parents.ContainsKey(iterNode))
            {
                previousNode = iterNode;
                iterNode = parents[iterNode];
            }

            return previousNode;
        }
    }
}