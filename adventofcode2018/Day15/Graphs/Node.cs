﻿using System;

namespace adventofcode2018.Day15.Graphs
{
    internal class Node : IEquatable<Node>
    {
        private int unitsInCell = 0;

        public Node(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }

        public int Y { get; }

        public bool IsVisitable => unitsInCell <= 0;

        internal void StepOut()
        {
            if (unitsInCell <= 0)
            {
                throw new InvalidOperationException($"Cannot step out from empty cell. {X}:{Y} Units in cell:{unitsInCell}");
            }
            unitsInCell--;
        }

        internal void StepInto()
        {
            if (unitsInCell > 0)
            {
                throw new InvalidOperationException($"Cannot step into non-empty cell. {X}:{Y} Units in cell:{unitsInCell}");
            }
            unitsInCell++;
        }

        internal int ReadingOrderCompareTo(Node other)
        {
            if (Y.CompareTo(other.Y) != 0)
            {
                return Y.CompareTo(other.Y);
            }
            else
            {
                return X.CompareTo(other.X);
            }
        }

        public override string ToString()
        {
            return $"{X}:{Y}, units in cell:{unitsInCell}";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Node);
        }

        public bool Equals(Node other)
        {
            return !(other is null) &&
                   X == other.X &&
                   Y == other.Y;
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public static bool operator==(Node obj1, Node obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }

            if (obj1 is null)
            {
                return false;
            }
            if (obj2 is null)
            {
                return false;
            }

            return obj1.X == obj2.X &&
                   obj1.Y == obj2.Y;
        }
        
        public static bool operator!=(Node obj1, Node obj2)
        {
            return !(obj1 == obj2);
        }
    }
}
