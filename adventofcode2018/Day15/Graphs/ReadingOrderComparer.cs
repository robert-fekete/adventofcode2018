﻿using System.Collections.Generic;
using adventofcode2018.Day15.Graphs;

namespace adventofcode2018.Day15.Graphs
{
    internal class ReadingOrderComparer : IComparer<Node>
    {
        public int Compare(Node x, Node y)
        {
            return x.ReadingOrderCompareTo(y);
        }
    }
}
