﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace adventofcode2018.Day15.Graphs
{
    internal class Graph
    {
        private readonly List<Node> nodes;
        private readonly Dictionary<Node, List<Node>> edges;

        public Graph(List<Node> nodes, Dictionary<Node, List<Node>> edges)
        {
            this.nodes = nodes;
            this.edges = edges;
        }

        public ResultBuilder FindClosestOfNodes(Node source, IEnumerable<Node> targets)
        {
            var queue = new Queue<(int, Node, Node)>();
            queue.Enqueue((0, source, null));

            var shortestPath = int.MaxValue;
            var builder = new ResultBuilder();

            var visited = new HashSet<Node>();
            while(queue.Count > 0)
            {
                (var distance, var current, var parent) = queue.Dequeue();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                if (!current.IsVisitable && current != source)
                {
                    continue;
                }

                // Visiting node
                builder.AddPathSegment(parent, current);

                if (targets.Contains(current))
                {
                    if (shortestPath < distance)
                    {
                        break;
                    }
                    else if (shortestPath == distance)
                    {
                        builder.AddNode(current);
                    }

                    if (shortestPath == int.MaxValue)
                    {
                        shortestPath = distance;
                        builder.AddNode(current);
                    }
                }

                foreach(var next in edges[current])
                {
                    queue.Enqueue((distance + 1, next, current));
                }
            }

            return builder;
        }

        public IEnumerable<Node> GetNodesInRange(Node source)
        {
            return edges[source];
        }

        public IEnumerable<Node> GetEmptyNodesInRange(Node source)
        {
            return edges[source].Where(edge => edge.IsVisitable);
        }

        internal void Print(BitmapPrinter printer)
        {
            foreach(var node in nodes)
            {
                printer.DrawPixel(node.X, node.Y, Color.Beige);
            }
        }
    }
}
