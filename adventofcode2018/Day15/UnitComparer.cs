﻿using System.Collections.Generic;

namespace adventofcode2018.Day15
{
    internal class UnitComparer : IComparer<Unit>
    {
        public int Compare(Unit a, Unit b)
        {
            return a.CompareTo(b);
        }
    }
}
