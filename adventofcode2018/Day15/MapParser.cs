﻿using System.Collections.Generic;
using System.Linq;
using adventofcode2018.Day15.Graphs;

namespace adventofcode2018.Day15
{
    internal class MapParser
    {
        private const char WALL = '#';
        private const char GOBLIN = 'G';
        private const char ELF = 'E';

        public Fight GetFight(IEnumerable<string> input, int goblinAttackPower, int elvAttackPower, int maxHealth, int id)
        {
            var map = input.ToArray();
            var nodes = new Dictionary<(int, int), Node>();
            var edges = new Dictionary<Node, List<Node>>();
            var units = new List<Unit>();

            var height = map.Length;
            var width = map[0].Length;
            for(int y = 0; y < height; y++)
            {
                for(int x = 0; x < width; x++)
                {
                    if(map[y][x] == WALL)
                    {
                        continue;
                    }
                    else
                    {
                        var node = new Node(x, y);
                        nodes[(x, y)] = node;
                        edges[node] = new List<Node>();
                        
                        if (map[y][x] == GOBLIN)
                        {
                            units.Add(new Unit(true, goblinAttackPower, maxHealth, node));
                        }
                        if(map[y][x] == ELF)
                        {
                            units.Add(new Unit(false, elvAttackPower, maxHealth, node));
                        }
                    }
                }
            }

            var diffs = new(int, int)[] { (0, -1), (-1, 0), (1, 0), (0, 1) };
            for(int y = 0; y < height; y++)
            {
                for(int x = 0; x < width; x++)
                {
                    var key = (x, y);
                    if (!nodes.ContainsKey(key))
                    {
                        continue;
                    }

                    var node1 = nodes[key];

                    foreach ((int dx, int dy) in diffs)
                    {
                        var x2 = x + dx;
                        var y2 = y + dy;
                        if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height || map[y2][x2] == WALL)
                        {
                            continue;
                        }

                        var node2 = nodes[(x2, y2)];
                        edges[node1].Add(node2);
                    }
                }
            }

            var graph = new Graph(nodes.Values.ToList(), edges);
            var printer = id == -1 ? null : new BitmapPrinter(width, height, 15, id);
            var fight = new Fight(graph, units, printer);

            return fight;
        }
    }
}
