﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day15
{
    internal class Solver
    {
        public int First(IEnumerable<string> input, int id = -1)
        {
            var parser = new MapParser();
            var fight = parser.GetFight(input, 3, 3, 200, id);

            fight.PlayUntilEnd();

            return fight.Outcome;
        }

        public int Second(IEnumerable<string> input, int id = -1)
        {
            var parser = new MapParser();

            var attackPower = BinarySearch(3, 200, i =>
            {
                var f = parser.GetFight(input, 3, i, 200, -1);
                f.PlayUntilEnd();
                return !f.AreElvesLost;
            });

            var fight = parser.GetFight(input, 3, attackPower, 200, id);
            fight.PlayUntilEnd();

            return fight.Outcome;
        }

        private int BinarySearch(int low, int high, Predicate<int> isSuccess)
        {
            while (high > low)
            {
                var mid = (high - low) / 2 + low;
                if (isSuccess(mid))
                {
                    high = mid;
                }
                else
                {
                    low = mid + 1;
                }
            }

            return high;
        }
    }
}
