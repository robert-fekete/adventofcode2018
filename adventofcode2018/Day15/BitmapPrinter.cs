﻿using System.Drawing;

namespace adventofcode2018.Day15
{
    internal class BitmapPrinter
    {
        private readonly int width;
        private readonly int height;
        private readonly int scale;
        private Bitmap bitmap;
        private int iter = 0;
        private int id = 0;

        public BitmapPrinter(int width, int height, int scale, int id)
        {
            this.width = width * scale;
            this.height = height * scale;
            this.scale = scale;
            this.id = id;
        }

        public void NewBitmap()
        {
            bitmap?.Dispose();
            bitmap = new Bitmap(width, height);

            for(int x = 0; x < width; x++)
            {
                for(int y = 0; y < height; y++)
                {
                    bitmap.SetPixel(x, y, Color.Brown);
                }
            }
            iter++;
        }

        public void DrawPixel(int x, int y, Color color)
        {
            for (int dx = 0; dx < scale; dx++)
            {
                for (int dy = 0; dy < scale; dy++)
                {
                    bitmap?.SetPixel(x * scale + dx, y * scale + dy, color);
                }
            }
        }

        public void SaveBitmap()
        {
            var path = $@"..\..\..\Day15_{id}_{iter}.jpg";
            bitmap.Save(path);
        }
    }
}
