﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using adventofcode2018.Day15.Graphs;

namespace adventofcode2018.Day15
{
    internal class Fight
    {
        private readonly Graph graph;
        private readonly IEnumerable<Unit> units;
        private readonly BitmapPrinter printer;
        private readonly int numberOfElves;

        private readonly ReadingOrderComparer nodeComparer = new ReadingOrderComparer();
        private readonly UnitComparer unitComparer = new UnitComparer();

        private int rounds = 0;
        private bool isEnded = false;

        public Fight(Graph graph, IEnumerable<Unit> units, BitmapPrinter printer)
        {
            this.graph = graph;
            this.units = units;
            this.printer = printer;
            this.numberOfElves = units.Count(u => !u.IsGoblin);
        }

        public int Outcome => rounds * units.Where(u => u.IsAlive).Select(u => u.HitPoints).Sum();
        public bool AreElvesLost => numberOfElves > units.Count(u => !u.IsGoblin && u.IsAlive);

        public void PlayUntilEnd()
        {
            while (!isEnded)
            {
                PlayRound();
            }
        }

        private void PlayRound()
        {
            if (isEnded)
            {
                return;
            }

            foreach(var unit in units.OrderBy(u => u.Node, nodeComparer))
            {
                if (!unit.IsAlive)
                {
                    continue;
                }

                // The first unit that doesn't have any living opponents left
                if (!units.Any(u => u.IsGoblin != unit.IsGoblin && u.IsAlive))
                {
                    isEnded = true;
                    Print();
                    return;
                }

                var enemiesInRange = GetEnemiesInRange(unit);
                if (!enemiesInRange.Any())
                {
                    UnitStep(unit);
                }

                UnitAttack(unit);
            }
            rounds++;

            Print();
        }

        private void UnitAttack(Unit unit)
        {
            var enemiesInRange = GetEnemiesInRange(unit);
            if (!enemiesInRange.Any())
            {
                return;
            }

            var enemyToAttack = enemiesInRange.OrderBy(u => u, unitComparer).First();
            unit.Attack(enemyToAttack);
        }

        private IEnumerable<Unit> GetEnemiesInRange(Unit unit)
        {
            var nodesInRange = graph.GetNodesInRange(unit.Node);
            var enemiesInRange = units.Where(u => u.IsAlive)
                .Where(u => u.IsGoblin != unit.IsGoblin)
                .Where(u => nodesInRange.Any(n => u.Node == n));

            return enemiesInRange;
        }

        private void UnitStep(Unit unit)
        {
            var nodesInRange = GetNodesInRange(unit);
            if (!nodesInRange.Any())
            {
                return;
            }

            var result = graph.FindClosestOfNodes(unit.Node, nodesInRange);
            if (!result.HasResult)
            {
                return;
            }

            var closest = result.GetClosestNode();
            var nextStep = result.GetFirstNodeInPathTo(closest);
            unit.StepTo(nextStep);
        }

        private IEnumerable<Node> GetNodesInRange(Unit unit)
        {
            return units.Where(u => u.IsGoblin != unit.IsGoblin)
                .Where(u => u.IsAlive)
                .SelectMany(u => graph.GetEmptyNodesInRange(u.Node))
                .Distinct();
        }

        private void Print()
        {
            //if(printer != null)
            //{
            //    printer.NewBitmap();

            //    graph.Print(printer);
            //    foreach(var unit in units.Where(u => u.IsAlive))
            //    {
            //        var color = unit.IsGoblin ? Color.Red : Color.Green;
            //        printer.DrawPixel(unit.Node.X, unit.Node.Y, color);
            //    }

            //    printer.SaveBitmap();
            //}
        }
    }
}
