﻿using System;
using adventofcode2018.Day15.Graphs;

namespace adventofcode2018.Day15
{
    internal class Unit
    {
        private readonly int attackPower;
        
        public Unit(bool isGoblin, int attackPower, int maxHealth, Node startingNode)
        {
            IsGoblin = isGoblin;
            this.attackPower = attackPower;
            HitPoints = maxHealth;

            Node = startingNode;
            startingNode.StepInto();
        }

        internal Node Node { get; private set; }
        
        internal bool IsGoblin;
        internal bool IsAlive => HitPoints > 0;
        internal int HitPoints { get; private set; }

        internal void StepTo(Node nextStep)
        {
            Node.StepOut();
            nextStep.StepInto();
            Node = nextStep;
        }

        internal void Attack(Unit enemyToAttack)
        {
            enemyToAttack.TakeDamage(attackPower);
        }

        private void TakeDamage(int damage)
        {
            HitPoints -= damage;
            if (HitPoints <= 0)
            {
                Die();
            }
        }

        private void Die()
        {
            Node.StepOut();
        }

        internal int CompareTo(Unit other)
        {
            if(HitPoints.CompareTo(other.HitPoints) != 0)
            {
                return HitPoints.CompareTo(other.HitPoints);
            }
            else
            {
                return Node.ReadingOrderCompareTo(other.Node);
            }
        }
    }
}