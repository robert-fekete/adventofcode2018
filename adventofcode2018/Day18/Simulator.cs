﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day18
{
    internal class Simulator
    {
        private Field field;

        public Simulator(Field field)
        {
            this.field = field;
        }

        public Checksum RunGenerations(int num)
        {
            for (int i = 0; i < num; i++)
            {
                field = field.RunGeneration();
            }

            return field.CheckSum;
        }

        public Checksum RunGenerationsWithHistory(long num)
        {
            var history = new List<Checksum>();
            for (int i = 0; i < num; i++)
            {
                history.Add(field.CheckSum);
                field = field.RunGeneration();

                if (TryCalculatePeriod(history, out int period, out int offset))
                {
                    var leftOver = (int)((num - offset) % period);
                    return history[offset + leftOver];
                }
            }

            return field.CheckSum;
        }

        private bool TryCalculatePeriod(List<Checksum> history, out int period, out int offset)
        {
            var first = history.Last();
            for (int i = 0; i < history.Count - 1; i++)
            {
                var second = history[i];

                if (first.CompareTo(second) == 0)
                {
                    period = history.Count - 1 - i;
                    offset = history.Count - 1 - period;
                    return true;
                }
            }

            offset = -1;
            period = -1;
            return false;
        }
    }
}
