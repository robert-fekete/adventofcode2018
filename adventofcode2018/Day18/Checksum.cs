﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day18
{
    internal class Checksum : IComparable<Checksum>
    {
        private readonly long[] trees;
        private readonly long[] lumberYards;

        private Checksum(long[] trees, long[] lumberYards)
        {
            this.trees = trees;
            this.lumberYards = lumberYards;
        }

        public int Calculate()
        {
            var noTrees = CalculateSubChecksum(trees);
            var noLumberYards = CalculateSubChecksum(lumberYards);

            return noTrees * noLumberYards;
        }

        private int CalculateSubChecksum(long[] values)
        {
            var checksum = 0;
            foreach (var value in values)
            {
                var temp = value;
                while (temp > 0)
                {
                    if (temp % 2 == 1)
                    {
                        checksum++;
                    }
                    temp /= 2;
                }
            }

            return checksum;
        }

        public static Checksum FromField(List<List<Acre>> acres)
        {
            var trees = GenerateSub(acres, a => a.IsTrees);
            var lumberYards = GenerateSub(acres, a => a.IsLumberYard);

            return new Checksum(trees, lumberYards);
        }

        private static long[] GenerateSub(List<List<Acre>> acres, Predicate<Acre> predicate)
        {
            var values = new List<long>();
            foreach(var line in acres)
            {
                long temp = 0;
                foreach(var acre in line)
                {
                    temp <<= 1;
                    if (predicate(acre))
                    {
                        temp |= 1;
                    }
                }
                values.Add(temp);
            }

            return values.ToArray();
        }

        public int CompareTo(Checksum other)
        {
            var comp1 = SubCompare(trees, other.trees);
            if (comp1 != 0)
            {
                return comp1;
            }

            var comp2 = SubCompare(lumberYards, other.lumberYards);
            if (comp2 != 0)
            {
                return comp2;
            }

            return 0;
        }

        private int SubCompare(long[] a, long[] b)
        {
            for (int i = 0; i < a.Length; i++)
            {
                var comp = a[i].CompareTo(b[i]);
                if (comp != 0)
                {
                    return comp;
                }
            }

            return 0;
        }
    }
}
