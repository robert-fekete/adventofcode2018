﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day18
{
    internal class AcreFactory
    {
        public Acre CreateNextGeneration(Acre previous, IEnumerable<Acre> adjacent)
        {
            if (previous.IsOpenGround)
            {
                if (adjacent.Count(a => a.IsTrees) >= 3)
                {
                    return Acre.NewTrees();
                }
                else
                {
                    return previous;
                }
            }
            else if (previous.IsTrees)
            {
                if (adjacent.Count(a => a.IsLumberYard) >= 3)
                {
                    return Acre.NewLumberYard();
                }
                else
                {
                    return previous;
                }
            }
            else if (previous.IsLumberYard)
            {
                if (adjacent.Count(a => a.IsLumberYard) >= 1 && adjacent.Count(a => a.IsTrees) >= 1)
                {
                    return previous;
                }
                else
                {
                    return Acre.NewOpenGround();
                }
            }
            else
            {
                throw new InvalidOperationException($"Invalid acre type");
            }
        }
    }
}
