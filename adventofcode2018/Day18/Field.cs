﻿using System.Collections.Generic;

namespace adventofcode2018.Day18
{
    internal class Field
    {
        private readonly AcreFactory factory = new AcreFactory();
        private readonly List<(int, int)> adjacentOffset = new List<(int, int)>
        {
            (-1, -1),
            (-1, 0),
            (-1, 1),
            (0, -1),
            (0, 1),
            (1, -1),
            (1, 0),
            (1, 1)
        };

        private List<List<Acre>> acres;

        private Field(List<List<Acre>> acres)
        {
            this.acres = acres;
        }
        public Checksum CheckSum => Checksum.FromField(acres);
        private int Size => acres.Count;

        public Field RunGeneration()
        {
            var nextAcres = new List<List<Acre>>();
            for (int y = 0; y < Size; y++)
            {
                var newLine = new List<Acre>();
                for (int x = 0; x < Size; x++)
                {
                    var adjacents = GetAdjacent(x, y);
                    var next = factory.CreateNextGeneration(acres[y][x], adjacents);

                    newLine.Add(next);
                }
                nextAcres.Add(newLine);
            }

            return new Field(nextAcres);
        }

        private IEnumerable<Acre> GetAdjacent(int x, int y)
        {
            var adjacent = new List<Acre>();
            foreach ((var dy, var dx) in adjacentOffset)
            {
                var newX = x + dx;
                var newY = y + dy;
                if (InRange(newX) && InRange(newY))
                {
                    adjacent.Add(acres[newY][newX]);
                }
            }

            return adjacent;
        }

        private bool InRange(int value)
        {
            return 0 <= value && value < Size;
        }

        public static Field FromInput(IEnumerable<string> input)
        {
            var fields = new List<List<Acre>>();
            foreach (var line in input)
            {
                var newLine = new List<Acre>();

                foreach (var c in line)
                {
                    newLine.Add(Acre.FromSymbol(c));
                }

                fields.Add(newLine);
            }

            return new Field(fields);
        }
    }
}
