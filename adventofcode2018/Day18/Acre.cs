﻿using System;

namespace adventofcode2018.Day18
{
    internal class Acre
    {
        private Type type;

        private Acre(Type type)
        {
            this.type = type;
        }

        public bool IsOpenGround => type == Type.OpenGround;
        public bool IsTrees => type == Type.Trees;
        public bool IsLumberYard => type == Type.LumberYard;

        public static bool operator ==(Acre a, Acre b)
        {
            return a.type == b.type;
        }

        public static bool operator!=(Acre a, Acre b)
        {
            return a.type != b.type;
        }

        public static Acre FromSymbol(char symbol)
        {
            switch (symbol)
            {
                case '.':
                    return new Acre(Type.OpenGround);
                case '|':
                    return new Acre(Type.Trees);
                case '#':
                    return new Acre(Type.LumberYard);
                default:
                    throw new InvalidOperationException($"Invalid Acre type: {symbol}");
            }
        }

        public static Acre NewLumberYard()
        {
            return new Acre(Type.LumberYard);
        }

        public static Acre NewOpenGround()
        {
            return new Acre(Type.OpenGround);
        }

        public static Acre NewTrees()
        {
            return new Acre(Type.Trees);
        }

        private enum Type
        {
            OpenGround,
            Trees,
            LumberYard
        }
    }
}
