﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day18
{
    internal class Solver
    {
        public int First(IEnumerable<string> input)
        {
            var field = Field.FromInput(input);
            var simulator = new Simulator(field);
            var checksum = simulator.RunGenerations(10);

            return checksum.Calculate();
        }

        public int Second(IEnumerable<string> input)
        {
            var field = Field.FromInput(input);
            var simulator = new Simulator(field);
            var checksum = simulator.RunGenerationsWithHistory(1000000000L);

            return checksum.Calculate();
        }
    }
}
