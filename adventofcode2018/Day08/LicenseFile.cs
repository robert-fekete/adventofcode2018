﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day08
{
    internal class LicenseFile
    {
        private readonly IReadOnlyList<int> license;
        private int iter;

        public LicenseFile(IReadOnlyList<int> license)
        {
            this.license = license;
        }

        public int GetMetadataSum()
        {
            iter = 0;

            return GetMetadataSumRecursive();
        }

        private int GetMetadataSumRecursive()
        {
            int sum = 0;

            var numberOfChildren = license[iter++];
            var numberOfMetadata = license[iter++];

            for (int i = 0; i < numberOfChildren; i++)
            {
                sum += GetMetadataSumRecursive();
            }

            for (int i = 0; i < numberOfMetadata; i++)
            {
                sum += license[iter++];
            }

            return sum;
        }

        public int GetRootValue()
        {
            iter = 0;

            return GetValuesRecursive();
        }

        private int GetValuesRecursive()
        {
            int sum = 0;

            var numberOfChildren = license[iter++];
            var numberOfMetadata = license[iter++];

            var childValues = new List<int>();
            for (int i = 0; i < numberOfChildren; i++)
            {
                childValues.Add(GetValuesRecursive());
            }

            if (numberOfChildren == 0)
            {
                for (int i = 0; i < numberOfMetadata; i++)
                {
                    sum += license[iter++];
                }
            }
            else
            {
                for(int i = 0; i < numberOfMetadata; i++)
                {
                    var metadata = license[iter++];
                    if (metadata > 0 && metadata <= numberOfChildren)
                    {
                        sum += childValues[metadata - 1];
                    }
                }
            }

            return sum;
        }
    }
}
