﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day08
{
    internal class Solver
    {
        public int First(string input)
        {
            var licenseData = input.Split(' ').Select(d => int.Parse(d)).ToList();
            var license = new LicenseFile(licenseData);

            return license.GetMetadataSum();
        }

        public int Second(string input)
        {
            var licenseData = input.Split(' ').Select(d => int.Parse(d)).ToList();
            var license = new LicenseFile(licenseData);

            return license.GetRootValue();
        }
    }
}
