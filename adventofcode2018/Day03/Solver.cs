﻿using System.Collections.Generic;

namespace adventofcode2018.Day03
{
    public class Solver
    {
        public int First(IEnumerable<string> input)
        {
            var tracker = new OverlapTracker();

            foreach (var line in input)
            {
                var area = ParseArea(line);
                tracker.AddArea(area);
            }

            return tracker.Size;
        }

        public int Second(IEnumerable<string> input)
        {
            var tracker = new OverlapTracker();

            foreach (var line in input)
            {
                var area = ParseArea(line);
                tracker.AddArea(area);
            }

            var uniqueArea = tracker.GetUniqueArea();

            return uniqueArea.Id;
        }

        private static Area ParseArea(string line)
        {
            var parts = line.Split(' ');

            var id = int.Parse(parts[0].Trim('#'));

            var topLeft = parts[2].Trim(' ', ':');
            var topLeftParts = topLeft.Split(',');
            int x = int.Parse(topLeftParts[0]);
            int y = int.Parse(topLeftParts[1]);

            var size = parts[3];
            var sizeParts = size.Split('x');
            int width = int.Parse(sizeParts[0]);
            int height = int.Parse(sizeParts[1]);

            return new Area(id, x, y, width, height);
        }
    }
}
