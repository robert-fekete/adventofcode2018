﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day03
{
    internal class Area : IEquatable<Area>
    {
        internal Area(int x, int y, int width, int height) :
            this(0, x, y, width, height)
        { }

        public Area(int id, int x, int y, int width, int height)
        {
            Id = id;
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public int Id { get; }
        private int X { get; }
        private int Y { get; }
        private int Width { get; }
        private int Height { get; }

        public bool HasOverlap(Area other)
        {
            return CommutativeHasRangeOverlap(X, X + Width, other.X, other.X + other.Width) &&
                CommutativeHasRangeOverlap(Y, Y + Height, other.Y, other.Y + other.Height);
        }

        internal IEnumerable<Point> GetOverlap(Area other)
        {
            var fromX = Math.Max(X, other.X);
            var toX = Math.Min(X + Width, other.X + other.Width);
            var fromY = Math.Max(Y, other.Y);
            var toY = Math.Min(Y + Height, other.Y + other.Height);

            for(int x = fromX; x < toX; x++)
            {
                for(int y = fromY; y < toY; y++)
                {
                    yield return new Point(x, y);
                }
            }
        }

        private bool CommutativeHasRangeOverlap(int a1, int a2, int b1, int b2)
        {
            return HasRangeOverlap(a1, a2, b1, b2) ||
                HasRangeOverlap(b1, b2, a1, a2);
        }

        private bool HasRangeOverlap(int a1, int a2, int b1, int b2)
        {
            var result = (a1 <= b1 && a2 > b1) ||
                (a2 >= b2 && a1 < b2);
            return result;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Area);
        }

        public bool Equals(Area other)
        {
            return other != null &&
                   Id == other.Id &&
                   X == other.X &&
                   Y == other.Y &&
                   Width == other.Width &&
                   Height == other.Height;
        }
    }
}
