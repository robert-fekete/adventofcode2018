﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day03
{
    internal class OverlapTracker
    {
        private readonly List<Area> areas = new List<Area>();
        private readonly HashSet<Point> overlap = new HashSet<Point>();

        public void AddArea(Area newArea)
        {
            foreach (var area in areas)
            {
                if (area.HasOverlap(newArea))
                {
                    var points = area.GetOverlap(newArea);
                    overlap.UnionWith(points);
                }
            }

            areas.Add(newArea);
        }

        public int Size => overlap.Count;

        internal Area GetUniqueArea()
        {
            foreach (var area1 in areas)
            {
                if (areas.All(area2 => area1.Equals(area2) || 
                        !area1.HasOverlap(area2))){
                    return area1;
                }
            }

            throw new Exception("Couldn't find non-overlapping area");
        }
    }
}
