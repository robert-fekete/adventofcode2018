﻿namespace adventofcode2018.Day09
{
    internal class CircularDoubleLinkedList<T>
    {
        private Node<T> current;

        public CircularDoubleLinkedList(T firstData)
        {
            current = new Node<T>(firstData);
        }

        public void MoveNext(int n)
        {
            for (int i = 0; i < n; i++)
            {
                current = current.Next;
            }
        }

        public void MovePrevious(int n)
        {
            for (int i = 0; i < n; i++)
            {
                current = current.Previous;
            }
        }

        public void AddNext(T data)
        {
            current.InsertNext(data);
            current = current.Next;
        }

        public T DeleteCurrent()
        {
            var temp = current;
            current = temp.Next;

            temp.Delete();
            return temp.Data;
        }

        public void Reset(T data)
        {
            current = new Node<T>(data);
        }

        public void Print()
        {
            var first = current;
            System.Console.Write($"{first.Data} ");

            var iter = current.Next;
            while (iter != first)
            {
                System.Console.Write($"{iter.Data} ");
                iter = iter.Next;
            }
            System.Console.WriteLine("");
        }
    }
}
