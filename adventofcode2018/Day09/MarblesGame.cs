﻿using System.Linq;

namespace adventofcode2018.Day09
{
    internal class MarblesGame
    {
        private readonly CircularDoubleLinkedList<long> marbles = new CircularDoubleLinkedList<long>(0);
        private readonly int SCORING_TURN_INDEX;

        public MarblesGame(int scoringTurnIndex)
        {
            this.SCORING_TURN_INDEX = scoringTurnIndex;
        }
        public long Play(int numberOfPlayers, int lastMarble)
        {
            var scoreBook = new long[numberOfPlayers];
            var turns = 0L;

            while(turns <= lastMarble)
            {
                var scoringTurn = (turns % SCORING_TURN_INDEX) == 0;
                if (scoringTurn)
                {
                    var player = turns % numberOfPlayers;
                    marbles.MovePrevious(7);

                    var value = marbles.DeleteCurrent();
                    var score = turns + value;
                    scoreBook[player] += score;

                    System.Console.WriteLine($"{player} is scoring {score}");
                }
                else
                {
                    marbles.MoveNext(1);
                    marbles.AddNext(turns);
                }

                turns++;
                //marbles.Print();
            }

            return scoreBook.Max();
        }
    }
}
