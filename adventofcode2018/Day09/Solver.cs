﻿namespace adventofcode2018.Day09
{
    internal class Solver
    {
        private const int PLAYER_INDEX = 0;
        private const int LAST_MARBLE_INDEX = 6;

        public long First(string input)
        {
            (var players, var lastMarble) = Parse(input);

            return Solve(players, lastMarble);
        }

        public long Second(string input)
        {
            (var players, var lastMarble) = Parse(input);

            return Solve(players, lastMarble * 100);
        }

        private (int, int) Parse(string input)
        {
            var parts = input.Split(' ');
            var players = int.Parse(parts[PLAYER_INDEX]);
            var lastMarble = int.Parse(parts[LAST_MARBLE_INDEX]);

            return (players, lastMarble);
        }

        private long Solve(int players, int lastMarble)
        {
            var game = new MarblesGame(23);
            var maxScore = game.Play(players, lastMarble);

            return maxScore;
        }
    }
}
