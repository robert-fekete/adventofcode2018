﻿namespace adventofcode2018.Day09
{
    internal class Node<T>
    {
        public Node(T data)
        {
            Next = this;
            Previous = this;

            Data = data;
        }

        public T Data { get; }
        internal Node<T> Next { get; private set; }
        internal Node<T> Previous { get; private set; }

        public void InsertNext(T newData)
        {
            var newNode = new Node<T>(newData);
            var temp = Next;
            temp.Previous = newNode;
            newNode.Next = temp;

            Next = newNode;
            newNode.Previous = this;
        }

        public void InsertPrevious(Node<T> newNode)
        {
            var temp = Previous;
            temp.Next = newNode;
            newNode.Previous = temp;

            Previous = newNode;
            newNode.Next = this;
        }

        public void Delete()
        {
            Next.Previous = Previous;
            Previous.Next = Next;

            Next = this;
            Previous = this;
        }
    }
}
