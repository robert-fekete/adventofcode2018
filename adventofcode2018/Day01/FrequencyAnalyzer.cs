﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day01
{
    internal class FrequencyAnalyzer
    {
        private IEnumerable<int> frequencies;

        public FrequencyAnalyzer(IEnumerable<int> frequencies)
        {
            this.frequencies = frequencies;
        }

        internal int GetResult()
        {
            var accumulator = 0;
            foreach(var frequency in frequencies)
            {
                accumulator += frequency;
            }

            return accumulator;
        }

        internal int GetFirstRepeat()
        {
            var previousFrequencies = new HashSet<int>();
            var accumulator = 0;

            while (true)
            {
                foreach(var frequency in frequencies)
                {
                    accumulator += frequency;

                    if (previousFrequencies.Contains(accumulator))
                    {
                        return accumulator;
                    }

                    previousFrequencies.Add(accumulator);
                }
            }
        }
    }
}
