﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day01
{
    internal class Solver
    {
        public int First(IEnumerable<string> input)
        {
            var frequencies = Parse(input);
            var analyzer = new FrequencyAnalyzer(frequencies);

            var resultingFrequency = analyzer.GetResult();
            return resultingFrequency;
        }

        public int Second(IEnumerable<string> input)
        {
            var frequencies = Parse(input);
            var analyzer = new FrequencyAnalyzer(frequencies);

            var resultingFrequency = analyzer.GetFirstRepeat();
            return resultingFrequency;
        }

        private static IEnumerable<int> Parse(IEnumerable<string> input)
        {
            return input.Select(line => int.Parse(line));
        }
    }
}
