﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace adventofcode2018.Day02
{
    internal class IdFilter
    {
        private readonly string[] input;

        public IdFilter(IEnumerable<string> input)
        {
            this.input = input.ToArray();
        }

        public string Filter()
        {
            (var a, var b) = GetCorrectIds();

            var builder = new StringBuilder();
            for(int i = 0; i < a.Length; i++)
            {
                if (a[i] == b[i])
                {
                    builder.Append(a[i]);
                }
            }

            return builder.ToString();
        }

        private (string, string) GetCorrectIds()
        {
            for (int i = 0; i < input.Length; i++)
            {
                for(int j = 0; j < input.Length; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }

                    if(AreIdsCorrect(input[i], input[j]))
                    {
                        return (input[i], input[j]);
                    }
                }
            }

            throw new InvalidOperationException("No correct IDs were found in the input");
        }

        private bool AreIdsCorrect(string a, string b)
        {
            int errors = 0;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i])
                {
                    errors++;
                    if (errors >= 2)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
