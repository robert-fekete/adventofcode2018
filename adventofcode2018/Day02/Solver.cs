﻿using System.Collections.Generic;

namespace adventofcode2018.Day02
{
    internal class Solver
    {
        public int First(IEnumerable<string> input)
        {
            var checksum = new Checksum(input);

            return checksum.Calculate();
        }

        public string Second(IEnumerable<string> input)
        {
            var filter = new IdFilter(input);

            return filter.Filter();
        }
    }
}
