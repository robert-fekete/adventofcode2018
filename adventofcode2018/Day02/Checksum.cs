﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day02
{
    internal class Checksum
    {
        private readonly IEnumerable<string> input;

        public Checksum(IEnumerable<string> input)
        {
            this.input = input;
        }

        public int Calculate()
        {
            var twos = 0;
            var threes = 0;
            foreach(var id in input)
            {
                var buckets = new Dictionary<char, int>();
                foreach(var c in id)
                {
                    if (!buckets.ContainsKey(c))
                    {
                        buckets[c] = 0;
                    }
                    buckets[c]++;
                }

                if (buckets.Any(kvp => kvp.Value == 2))
                {
                    twos++;
                }
                if (buckets.Any(kvp => kvp.Value == 3))
                {
                    threes++;
                }
            }

            return twos * threes;
        }
    }
}
