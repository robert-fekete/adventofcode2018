﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day24
{
    class Parser
    {
        public (Army, Army) ParseInput(IEnumerable<string> input, int immuneBoost = 0)
        {
            var immuneArmy = ParseArmy(input.TakeWhile(l => l != string.Empty), immuneBoost);
            var infectionArmy = ParseArmy(input.SkipWhile(l => l != string.Empty).Skip(1));

            return (immuneArmy, infectionArmy);
        }

        private Army ParseArmy(IEnumerable<string> input, int immuneBoost = 0)
        {
            var groups = new List<Group>();
            foreach(var line in input.Skip(1))
            {
                var group = ParseGroup(line, groups.Count + 1, immuneBoost);
                groups.Add(group);
            }

            return new Army(groups);
        }

        private Group ParseGroup(string line, int id, int immuneBoost)
        {
            var parts = line.Split(' ');
            var number = int.Parse(parts[0]);
            var health = int.Parse(parts[4]);
            var damage = int.Parse(FindTokenWithOffset(parts, "does", 1));
            var initiative = int.Parse(FindTokenWithOffset(parts, "initiative", 1));
            var attackType = FindTokenWithOffset(parts, "damage", -1);

            string[] weaknesses = new string[0];
            string[] immunities = new string[0];
            if (line.Contains('('))
            {
                var weaknessAndImmunity = line.Split('(')[1].Split(')')[0];
                var weaknessAndImmunityParts = weaknessAndImmunity.Split(';').Select(s => s.Trim());
                weaknesses = ParseWeaknessAndImmunity(weaknessAndImmunityParts, "weak");
                immunities = ParseWeaknessAndImmunity(weaknessAndImmunityParts, "immune");
            }

            return new Group(id, number, health, damage + immuneBoost, attackType, initiative, weaknesses, immunities);
        }

        private static string[] ParseWeaknessAndImmunity(IEnumerable<string> weaknessAndImmunityParts, string name)
        {
            return weaknessAndImmunityParts.Where(p => p.StartsWith(name))
                                                        .SelectMany(s => s.Split(' '))
                                                        .Skip(2)
                                                        .Select(s => s.Trim(',')).ToArray();
        }

        private string FindTokenWithOffset(string[] tokens, string token, int offset)
        {
            for(int i = 0; i < tokens.Length; i++)
            {
                if (tokens[i] == token)
                {
                    return tokens[i + offset];
                }
            }

            throw new InvalidOperationException("Token not found");
        }
    }
}
