﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day24
{
    class Army
    {
        private IEnumerable<Group> groups;

        public Army(IEnumerable<Group> groups)
        {
            this.groups = groups;
        }

        public bool IsDefeated => groups.All(g => !g.IsAlive);
        public IEnumerable<Group> RemainingGroups => groups.Where(g => g.IsAlive);
        public long RemainingUnits => groups.Sum(g => g.RemainingUnits);

        public Dictionary<Group, Group> SelectTargets(Army attackerArmy)
        {
            var prioritizedGroups = attackerArmy.RemainingGroups
                                    .OrderByDescending(g => g.EffectivePower)
                                    .ThenByDescending(g => g.Initiative);

            var targetedGroups = new HashSet<Group>();

            var targets = new Dictionary<Group, Group>();
            foreach(var attackerGroup in prioritizedGroups)
            {
                var availableTargets = RemainingGroups.Where(g => !targetedGroups.Contains(g))
                                        .Select(g => (g, attackerGroup.CalculateDamageTo(g)))
                                        .Where(p => p.Item2 > 0)
                                        .OrderByDescending(p => p.Item2)
                                        .ThenByDescending(p => p.g.EffectivePower)
                                        .ThenByDescending(p => p.g.Initiative);

                if (availableTargets.Any())
                {
                    var target = availableTargets.First().g;
                    targets[attackerGroup] = target;
                    targetedGroups.Add(target);
                }
            }

            return targets;
        }

        public override string ToString()
        {
            return string.Join("\n", groups.Select(g => g.ToString()));
        }
    }
}
