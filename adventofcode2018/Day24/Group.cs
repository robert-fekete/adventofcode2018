﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day24
{
    class Group
    {
        private int number;
        private int health;
        private int damage;
        private string attackType;
        private string[] weaknesses;
        private string[] immunities;

        public Group(int id, int number, int health, int damage, string attackType, int initiative, string[] weaknesses, string[] immunities)
        {
            Id = id;
            this.number = number;
            this.health = health;
            this.damage = damage;
            this.attackType = attackType;
            Initiative = initiative;
            this.weaknesses = weaknesses;
            this.immunities = immunities;
        }

        public int Id { get; }
        public int Initiative { get; }
        public bool IsAlive => number > 0;
        public int EffectivePower => number * damage;
        public long RemainingUnits => number;


        public int CalculateDamageTo(Group target)
        {
            if (target.immunities.Contains(attackType))
            {
                return 0;
            }

            var damageFactor = 1;
            if (target.weaknesses.Contains(attackType))
            {
                damageFactor = 2;
            }

            var damage = EffectivePower * damageFactor;
            return damage;
        }

        public void TakeDamage(int damage)
        {
            var wholeUnits = damage / health;
            var unitsToDie = Math.Min(wholeUnits, number);
            number -= unitsToDie;

            //Console.WriteLine($"{unitsToDie} units died.");
        }

        public override string ToString()
        {
            var weaknessesAndImmunities = new List<string>();
            if (weaknesses.Any())
            {
                weaknessesAndImmunities.Add("weak to " + string.Join(", ", weaknesses));
            }
            if (immunities.Any())
            {
                weaknessesAndImmunities.Add("immune to " + string.Join(", ", immunities));
            }

            return $"Group {Id}: {number} units each with {health} hit points ({string.Join("; ", weaknessesAndImmunities)}) with an attack that does {damage} {attackType} damage at initiative {Initiative}";
        }
    }
}
