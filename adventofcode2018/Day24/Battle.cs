﻿using System.Linq;

namespace adventofcode2018.Day24
{
    class Battle
    {
        private readonly Army immune;
        private readonly Army infection;

        public Battle(Army immune, Army infection)
        {
            this.immune = immune;
            this.infection = infection;
        }

        public bool IsNeverEnding { get; private set; } = false;
        public bool IsEnded => immune.IsDefeated || infection.IsDefeated;
        public Army Winner => immune.IsDefeated ? infection : immune;

        public void Fight()
        {
            var immuneTargets = infection.SelectTargets(immune);
            var infectionTargets = immune.SelectTargets(infection);

            var attackingGroups = immuneTargets.Keys.Concat(infectionTargets.Keys).OrderByDescending(g => g.Initiative);

            var immuneRemainingUnits = immune.RemainingUnits;
            var infectionRemainingUnits = infection.RemainingUnits;

            foreach(var attacker in attackingGroups)
            {
                if (!attacker.IsAlive)
                {
                    continue;
                }

                var target = immuneTargets.ContainsKey(attacker) ? immuneTargets[attacker] : infectionTargets[attacker];
                var damage = attacker.CalculateDamageTo(target);

                var attackerType = immuneTargets.ContainsKey(attacker) ? "Immune System" : "Infection";
                var targetType = !immuneTargets.ContainsKey(attacker) ? "Immune System" : "Infection";
                //Console.WriteLine($"{attackerType} group {attacker.Id} targets {targetType} group {target.Id} and deals {damage} damage.");

                target.TakeDamage(damage);
            }
            //Console.WriteLine();

            if (!IsEnded && immuneRemainingUnits == immune.RemainingUnits && infectionRemainingUnits == infection.RemainingUnits)
            {
                // Stop the battle if it will never end
                IsNeverEnding = true;
            }
        }
    }
}
