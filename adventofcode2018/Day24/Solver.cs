﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day24
{
    class Solver
    {
        public long First(IEnumerable<string> input)
        {
            return RunBattle(input).Survivors;
        }

        public long Second(IEnumerable<string> input)
        {
            var minBoost = BinarySearch(0, 10000000, boost => RunBattle(input, boost).IsImmuneWin);

            Console.WriteLine(minBoost);
            return RunBattle(input, minBoost).Survivors;
        }

        private static (bool IsImmuneWin, long Survivors) RunBattle(IEnumerable<string> input, int immuneBoost = 0)
        {
            var parser = new Parser();
            (var immuneArmy, var infectionArmy) = parser.ParseInput(input, immuneBoost);


            var battle = new Battle(immuneArmy, infectionArmy);
            while (!battle.IsEnded && !battle.IsNeverEnding)
            {
                battle.Fight();
            }

            return (battle.IsEnded && immuneArmy.Equals(battle.Winner), battle.Winner.RemainingUnits);
        }

        private static int BinarySearch(int first, int last, Predicate<int> searchPredicate)
        {
            while (first < last)
            {
                var mid = first + (last - first) / 2;

                if (searchPredicate(mid))
                {
                    last = mid;
                }
                else
                {
                    first = mid + 1;
                }
            }

            return first;
        }
    }
}
