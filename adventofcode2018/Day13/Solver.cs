﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day13
{
    internal class Solver
    {
        internal string First(IEnumerable<string> input)
        {
            var map = GetMap(input);
            var crashCoordinates = map.PlayUntilFirstCrash();

            return $"{crashCoordinates.Item1},{crashCoordinates.Item2}";
        }

        internal string Second(IEnumerable<string> input)
        {
            var map = GetMap(input);
            var crashCoordinates = map.PlayUntilLastCrash();

            return $"{crashCoordinates.Item1},{crashCoordinates.Item2}";
        }

        private Map GetMap(IEnumerable<string> input)
        {
            var parser = new MapParser();

            return parser.GetMap(input);
        }
    }
}
