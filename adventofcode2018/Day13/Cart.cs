﻿using System;

namespace adventofcode2018.Day13
{
    internal class Cart
    {
        private readonly int[] turns = { 3, 0, 1 };
        private int rawDirection;
        private int turnIndex; // 1 - left, 2 - straigt, 3 - right
        private int lastMove;

        public Cart(int rawDirection)
            : this(rawDirection, 0, 0)
        { }

        public Cart(int rawDirection, int nextTurn, int lastMove)
        {
            this.rawDirection = rawDirection;
            this.turnIndex = nextTurn % 3;
            this.lastMove = lastMove;
        }
        public void ResetProperties(int rawDirection, int nextTurn, int lastMove)
        {
            this.rawDirection = rawDirection;
            this.turnIndex = nextTurn % 3;
            this.lastMove = lastMove;
        }

        public int RawValue => rawDirection;

        public bool CanMove(int tick)
        {
            return tick > lastMove;
        }

        public void SetNewDirection(char turnType, int tick)
        {
            if (turnType == '-' || turnType == '|')
            {
                ResetProperties(rawDirection, turnIndex, tick);
            }
            else if (turnType == '+')
            {
                var newRawDirection = (rawDirection + turns[turnIndex]) % 4;
                ResetProperties(newRawDirection, turnIndex + 1, tick);
            }
            else if (turnType == '/')
            {
                if (rawDirection == 0)
                {
                    ResetProperties(1, turnIndex, tick);
                }
                else if (rawDirection == 1)
                {
                    ResetProperties(0, turnIndex, tick);
                }
                else if (rawDirection == 2)
                {
                    ResetProperties(3, turnIndex, tick);
                }
                else if (rawDirection == 3)
                {
                    ResetProperties(2, turnIndex, tick);
                }
                else
                {
                    throw new Exception($"Arriving to a turn from the wrong direction: {rawDirection}, {turnType}");
                }
            }
            else if (turnType == '\\')
            {
                if (rawDirection == 0)
                {
                    ResetProperties(3, turnIndex, tick);
                }
                else if (rawDirection == 1)
                {
                    ResetProperties(2, turnIndex, tick);
                }
                else if (rawDirection == 2)
                {
                    ResetProperties(1, turnIndex, tick);
                }
                else if (rawDirection == 3)
                {
                    ResetProperties(0, turnIndex, tick);
                }
                else
                {
                    throw new Exception($"Arriving to a turn from the wrong direction: {rawDirection}, {turnType}");
                }
            }
            else
            {
                throw new Exception($"Invalid turn type {turnType}");
            }
        }

        public void Print()
        {
            switch (rawDirection)
            {
                case 0:
                    Console.Write('^');
                    break;
                case 1:
                    Console.Write('>');
                    break;
                case 2:
                    Console.Write('v');
                    break;
                case 3:
                    Console.Write('<');
                    break;
                default:
                    throw new Exception($"Invalid raw direction {rawDirection}");
            }
        }
    }
}
