﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace adventofcode2018.Day13
{
    [DebuggerDisplay("{x}:{y}")]
    internal class Node
    {
        private readonly int x;
        private readonly int y;
        private readonly char type;
        private int num = 0;

        private readonly List<Cart> cartsToRemove = new List<Cart>();
        private readonly List<Cart> carts = new List<Cart>();
        private Node top;
        private Node right;
        private Node bottom;
        private Node left;

        public Node(int x, int y, char type)
        {
            this.x = x;
            this.y = y;
            this.type = type;
        }

        public bool HasCrash => NumberOfCarts > 1;
        public int NumberOfCarts
        {
            get
            {
                return num;
            }
        }

        internal void AddBottomNeighbor(Node node)
        {
            if (CanAddBottomNeighbor())
            {
                bottom = node;
                node.AddTopNeighbor(this);
            }
        }

        public bool CanAddBottomNeighbor()
        {
            return type == '|' ||
                type == '+' ||
                (type == '/' && top == null) ||
                (type == '\\' && top == null);
        }

        private void AddTopNeighbor(Node node)
        {
            top = node;
        }

        internal void AddRightNeighbor(Node node)
        {
            if (CanAddRightNeighbor())
            {
                right = node;
                node.AddLeftNeighbor(this);
            }
        }

        public bool CanAddRightNeighbor()
        {
            return type == '-' ||
                type == '+' ||
                (type == '/' && left == null) ||
                (type == '\\' && left == null);
        }

        private void AddLeftNeighbor(Node node)
        {
            left = node;
        }

        internal void AddCart(Cart cartDirection)
        {
            carts.Add(cartDirection);
            num++;
        }

        internal void MoveCarts(int tick)
        {
            RemoveCarts();

            cartsToRemove.Clear();
            foreach(var cart in carts)
            {
                if (cart.CanMove(tick))
                {
                    cart.SetNewDirection(type, tick);
                    MoveCart(cart);
                    cartsToRemove.Add(cart);
                }
            }

            foreach(var cart in cartsToRemove)
            {
                carts.Remove(cart);
            }
        }

        internal void RemoveCarts()
        {
            if (HasCrash)
            {
                carts.Clear();
                num = 0;
                return;
            }
        }

        private void MoveCart(Cart direction)
        {
            num--;
            if (direction.RawValue == 0)
            {
                top.AddCart(direction);
            }
            else if (direction.RawValue == 1)
            {
                right.AddCart(direction);
            }
            else if (direction.RawValue == 2)
            {
                bottom.AddCart(direction);
            }
            else if (direction.RawValue == 3)
            {
                left.AddCart(direction);
            }
            else
            {
                throw new Exception($"Invalid raw direction {direction.RawValue}");
            }
        }

        internal void Print()
        {
            if (carts.Count == 1)
            {
                carts[0].Print();
            }
            else if (carts.Count > 1)
            {
                Console.Write('X');
            }
            else
            {
                if (top != null && right != null && bottom == null && left == null)
                {
                    Console.Write('\\');
                    return;
                }
                if (top == null && right == null && bottom != null && left != null)
                {
                    Console.Write('\\');
                    return;
                }
                if (top != null && right == null && bottom == null && left != null)
                {
                    Console.Write('/');
                    return;
                }
                if (top == null && right != null && bottom != null && left == null)
                {
                    Console.Write('/');
                    return;
                }
                if (top != null && right == null && bottom != null && left == null)
                {
                    Console.Write('|');
                    return;
                }
                if (top == null && right != null && bottom == null && left != null)
                {
                    Console.Write('-');
                    return;
                }
                if (top != null && right != null && bottom != null && left != null)
                {
                    Console.Write('+');
                    return;
                }
                Console.Write('@');
            }
        }
    }
}
