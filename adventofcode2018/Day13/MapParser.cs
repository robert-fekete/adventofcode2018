﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day13
{
    internal class MapParser
    {
        private readonly char[] CART_CHARS = new char[] { '^', '>', 'v', '<' };

        private string[] input;
        private Dictionary<Coordinate, Node> nodes;
        
        public Map GetMap(IEnumerable<string> inputEnum)
        {
            nodes = new Dictionary<Coordinate, Node>();

            input = inputEnum.ToArray();

            var y = 0;
            var maxWidth = int.MinValue;
            var numberOfCarts = 0;
            foreach(var line in input)
            {
                var width = line.Length;
                if (width > maxWidth)
                {
                    maxWidth = width;
                }

                for(int x = 0; x < width; x++)
                {
                    if (line[x] == ' ')
                    {
                        continue;
                    }
                    if (CART_CHARS.Contains(line[x]))
                    {
                        numberOfCarts++;
                        AddCart(y, x, line[x]);
                    }
                    if (line[x] == '|')
                    {
                        AddVerticalTunnel(y, x);
                    }
                    if (line[x] == '-')
                    {
                        AddHorizontalTunnel(y, x);
                    }
                    if (line[x] == '+')
                    {
                        AddCrossing(y, x);
                    }
                    if (line[x] == '\\')
                    {
                        AddLeftTurn(y, x);
                    }
                    if (line[x] == '/')
                    {
                        AddRightTurn(y, x);
                    }
                }

                y++;
            }

            return new Map(maxWidth, y, numberOfCarts, nodes);
        }

        private void AddCart(int y, int x, char cart)
        {
            var hasTopNode = nodes.ContainsKey(new Coordinate(x, y - 1)) && nodes[new Coordinate(x, y - 1)].CanAddBottomNeighbor();
            var hasLeftNode = nodes.ContainsKey(new Coordinate(x - 1, y)) && nodes[new Coordinate(x - 1, y)].CanAddRightNeighbor();
            if (hasTopNode && hasLeftNode)
            {
                AddCrossing(y, x);
            }
            else if (hasTopNode)
            {
                AddVerticalTunnel(y, x);
            }
            else if (hasLeftNode)
            {
                AddHorizontalTunnel(y, x);
            }
            else
            {
                AddRightTurn(y, x);
            }

            var direction = new Cart(Array.IndexOf(CART_CHARS, cart));
            nodes[new Coordinate(x, y)].AddCart(direction);
        }

        private void AddVerticalTunnel(int y, int x)
        {
            var node = new Node(x, y, '|');
            nodes[new Coordinate(x, y)] = node;

            var neighbor = nodes[new Coordinate(x, y - 1)];
            neighbor.AddBottomNeighbor(node);
        }

        private void AddHorizontalTunnel(int y, int x)
        {
            var node = new Node(x, y, '-');
            nodes[new Coordinate(x, y)] = node;

            var neighbor = nodes[new Coordinate(x - 1, y)];
            neighbor.AddRightNeighbor(node);
        }

        private void AddRightTurn(int y, int x)
        {
            var node = new Node(x, y, '/');
            nodes[new Coordinate(x, y)] = node;

            if (nodes.ContainsKey(new Coordinate(x, y - 1)))
            {
                var top = nodes[new Coordinate(x, y - 1)];
                top.AddBottomNeighbor(node);
            }
            if (nodes.ContainsKey(new Coordinate(x - 1, y)))
            {
                var left = nodes[new Coordinate(x - 1, y)];
                left.AddRightNeighbor(node);
            }
        }

        private void AddLeftTurn(int y, int x)
        {
            var node = new Node(x, y, '\\');
            nodes[new Coordinate(x, y)] = node;

            if (nodes.ContainsKey(new Coordinate(x, y - 1)))
            {
                var top = nodes[new Coordinate(x, y - 1)];
                top.AddBottomNeighbor(node);
            }
            if (nodes.ContainsKey(new Coordinate(x - 1, y)))
            {
                var left = nodes[new Coordinate(x - 1, y)];
                left.AddRightNeighbor(node);
            }
        }

        private void AddCrossing(int y, int x)
        {
            var node = new Node(x, y, '+');
            nodes[new Coordinate(x, y)] = node;

            var top = nodes[new Coordinate(x, y - 1)];
            top.AddBottomNeighbor(node);
            var left = nodes[new Coordinate(x - 1, y)];
            left.AddRightNeighbor(node);
        }
    }
}
