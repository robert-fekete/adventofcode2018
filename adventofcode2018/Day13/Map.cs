﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day13
{
    internal class Map
    {
        private readonly int width;
        private readonly int height;
        private int numberOfCarts;
        private readonly Dictionary<Coordinate, Node> nodes;

        private int tick = 0;

        public Map(int width, int height, int numberOfCarts, Dictionary<Coordinate, Node> nodes)
        {
            this.width = width;
            this.height = height;
            this.numberOfCarts = numberOfCarts;
            this.nodes = nodes;
        }

        public (int, int) PlayUntilFirstCrash()
        {
            while (true)
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        var key = new Coordinate(x, y);
                        if (nodes.ContainsKey(key))
                        {
                            if (nodes[key].HasCrash)
                            {
                                return key.ToTuple();
                            }

                            nodes[key].MoveCarts(tick);
                        }
                    }
                }

                tick++;
            }
        }

        public (int, int) PlayUntilLastCrash()
        {
            var validNodes = new List<Node>();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var key = new Coordinate(x, y);
                    if (nodes.ContainsKey(key))
                    {
                        validNodes.Add(nodes[key]);
                    }
                }
            }

            while (true)
            {
                foreach(var node in validNodes)
                { 
                    if (node.HasCrash)
                    {
                        numberOfCarts -= node.NumberOfCarts;
                    }
                    node.MoveCarts(tick);
                }

                foreach(var node in validNodes)
                {
                    if (node.HasCrash)
                    {
                        numberOfCarts -= node.NumberOfCarts;
                        node.RemoveCarts();
                    }
                }

                tick++;
                if (numberOfCarts == 1)
                {
                    return nodes.Where(kvp => kvp.Value.NumberOfCarts == 1)
                        .Select(kvp => kvp.Key)
                        .First()
                        .ToTuple();
                }
            }
        }

        public void Print()
        {
            for(int y = 0; y < height; y++)
            {
                for(int x = 0; x < width; x++)
                {
                    var key = new Coordinate(x, y);
                    if (nodes.ContainsKey(key))
                    {
                        nodes[key].Print();
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }
                Console.WriteLine();
            }
        }
    }
}