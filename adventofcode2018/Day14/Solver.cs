﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day14
{
    internal class Solver
    {
        internal string First(string input)
        {
            var numberOfRecipes = int.Parse(input);
            var board = new RecipeBoard();

            var scores = board.GetScores(numberOfRecipes, 10);

            return scores;
        }

        internal int Second(string input)
        {
            var scores = GetScores(input);
            var board = new RecipeBoard();

            var numberOfRecipes = board.GetNumberOfRecipes(scores);

            return numberOfRecipes;
        }

        private int[] GetScores(string input)
        {
            var result = new List<int>();
            foreach(var c in input)
            {
                result.Add(c - '0');
            }

            return result.ToArray();
        }
    }
}
