﻿using System;
using System.Collections.Generic;
using System.Text;

namespace adventofcode2018.Day14
{
    internal class RecipeBoard
    {
        private int firstIter = 0;
        private int secondIter = 1;
        private List<int> recipes = new List<int>() { 3, 7 };

        public RecipeBoard()
        { }

        internal string GetScores(int numberOfRecipes, int numberOfScores)
        {
            MakeRecipes(numberOfRecipes + numberOfScores);

            return GetScoreSuffix(numberOfRecipes, numberOfScores);
        }

        private string GetScoreSuffix(int offset, int length)
        {
            var builder = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                var index = offset + i;
                builder.Append(recipes[index]);
            }

            return builder.ToString();
        }

        internal int GetNumberOfRecipes(int[] scores)
        {
            MakeRecipes(scores.Length);

            while(!MatchSuffix(scores) && !MatchSuffix(scores, 1))
            {
                MakeNewRecipe();
            }

            if (MatchSuffix(scores, 1))
            {
                return recipes.Count - scores.Length - 1;
            }
            else
            {
                return recipes.Count - scores.Length;
            }
        }

        private bool MatchSuffix(int[] scores, int offsetFromEnd = 0)
        {  
            var offset = recipes.Count - scores.Length - offsetFromEnd;
            if (offset < 0)
            {
                return false;
            }

            for (int i = 0; i < scores.Length; i++)
            {
                if (scores[i] != recipes[offset + i])
                {
                    return false;
                }
            }
            return true;
        }

        private void MakeRecipes(int minimumNumber)
        {
            while (recipes.Count < minimumNumber)
            {
                MakeNewRecipe();
            }
        }

        private void MakeNewRecipe()
        {
            var sum = recipes[firstIter] + recipes[secondIter];
            if (sum > 9)
            {
                recipes.Add(sum / 10);
            }
            recipes.Add(sum % 10);

            firstIter = (firstIter + 1 + recipes[firstIter]) % recipes.Count;
            secondIter = (secondIter + 1 + recipes[secondIter]) % recipes.Count;
        }
    }
}