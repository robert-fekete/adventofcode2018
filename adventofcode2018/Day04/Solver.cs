﻿using System.Collections.Generic;

namespace adventofcode2018.Day04
{
    internal class Solver
    {
        public int First(IEnumerable<string> input)
        {
            var collection = ParseInput(input);

            (var id, var time) = collection.FindBestSleepData1();

            return id * time;
        }

        public int Second(IEnumerable<string> input)
        {
            var collection = ParseInput(input);

            (var id, var time) = collection.FindBestSleepData2();

            return id * time;
        }

        private static GuardCollection ParseInput(IEnumerable<string> input)
        {
            var parser = new GuardsParser();
            var collection = parser.ParseGuardCollection(input);

            return collection;
        }
    }
}
