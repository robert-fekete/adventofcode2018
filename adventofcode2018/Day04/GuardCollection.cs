﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day04
{
    internal class GuardCollection
    {
        private readonly IEnumerable<Guard> guards;

        public GuardCollection(IEnumerable<Guard> guards)
        {
            this.guards = guards;
        }

        public (int id, int time) FindBestSleepData1()
        {
            var guard = FindGuardWithMostSleep();

            return (guard.Id, guard.GetMostPopularSleepTime());
        }

        public (int id, int time) FindBestSleepData2()
        {
            int bestTime = -1;
            int maxNoSleep = -1;
            int guardIdOfInterest = -1;
            for (int i = 0; i < 60; i++)
            {
                foreach(var guard in guards)
                {
                    var noSleep = guard.GetNumberOfSleepsWithTime(i);
                    if (noSleep > maxNoSleep)
                    {
                        maxNoSleep = noSleep;
                        guardIdOfInterest = guard.Id;
                        bestTime = i;
                    }
                }
            }

            return (guardIdOfInterest, bestTime);
        }

        private Guard FindGuardWithMostSleep()
        {
            var maxSleepTime = -1;
            Guard guardOfInterest = null;
            foreach(var guard in guards)
            {
                if (guard.TotalSleepLength > maxSleepTime)
                {
                    guardOfInterest = guard;
                    maxSleepTime = guard.TotalSleepLength;
                }
            }

            return guardOfInterest;
        }
    }
}
