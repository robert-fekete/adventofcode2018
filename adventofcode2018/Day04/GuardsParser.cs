﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day04
{
    internal class GuardsParser
    {
        public GuardCollection ParseGuardCollection(IEnumerable<string> input)
        {
            var entries = new List<(DateTime, string)> ();
            foreach(var line in input)
            {
                var parts = line.Split(']');
                var timestampToken = parts[0].Trim('[');
                var timeStamp = DateTime.Parse(timestampToken);

                var messageToken = parts[1].Trim();
                entries.Add((timeStamp, messageToken));
            }

            var orderedEntries = entries.OrderBy(entry => entry.Item1);

            int guardId = -1;
            int sleepStart = -1;
            Dictionary<int, Guard> guards = new Dictionary<int, Guard>();
            foreach ((var timeStamp, var message) in orderedEntries)
            {
                if (message.StartsWith("Guard"))
                {
                    var parts = message.Split(' ');
                    var idToken = parts[1].Trim('#');
                    guardId = int.Parse(idToken);
                    if (!guards.ContainsKey(guardId))
                    {
                        guards[guardId] = new Guard(guardId);
                    }
                }
                else if (message.StartsWith("falls"))
                {
                    sleepStart = timeStamp.Minute;
                }
                else
                {
                    if (sleepStart == -1)
                    {
                        throw new Exception("Guard is waking up without falling asleep");
                    }
                    if (guardId == -1)
                    {
                        throw new Exception("Guard is waking up without starting its shift");
                    }

                    var sleep = new SleepingSpan(sleepStart, timeStamp.Minute);
                    guards[guardId].AddSleep(sleep);
                }
            }

            return new GuardCollection(guards.Values);
        }
    }
}
