﻿using System.Diagnostics;

namespace adventofcode2018.Day04
{
    [DebuggerDisplay("{start} - {end}")]
    internal class SleepingSpan
    {
        private readonly int start;
        private readonly int end;

        public SleepingSpan(int start, int end)
        {
            this.start = start;
            this.end = end;
        }

        public int Length => end - start;

        public bool Contains(int time)
        {
            return start <= time && end > time;
        }
    }
}
