﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day04
{
    internal class Guard
    {
        private readonly List<SleepingSpan> sleeps = new List<SleepingSpan>();
        public Guard(int id)
        {
            Id = id;
        }

        public int Id { get; }

        public int TotalSleepLength => sleeps.Sum(sleep => sleep.Length);

        public void AddSleep(SleepingSpan sleep)
        {
            sleeps.Add(sleep);
        }

        public int GetMostPopularSleepTime()
        {
            int time = 0;
            int maxCount = 0;
            for(int i = 0; i < 60; i++)
            {
                var count = sleeps.Count(sleep => sleep.Contains(i));
                if (count > maxCount)
                {
                    maxCount = count;
                    time = i;
                }
            }

            return time;
        }

        public int GetNumberOfSleepsWithTime(int time)
        {
            return sleeps.Count(sleep => sleep.Contains(time));
        }
    }
}
