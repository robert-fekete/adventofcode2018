﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace adventofcode2018.Day23
{
    class Solver
    {
        internal object First(IEnumerable<string> input)
        {
            var parser = new Parser();
            var nanobots = parser.ParseNanobots(input);

            var strongest = nanobots.OrderByDescending(n => n.Range).First();

            var nanobotsInRange = nanobots.Count(n => strongest.IsInRange(n));

            return nanobotsInRange;
        }

        // Not a good solution as it wouldn't return the correct answer for every kind of inputs
        internal object Second(IEnumerable<string> input)
        {
            var parser = new Parser();
            var nanobots = parser.ParseNanobots(input);

            var octahedrons = nanobots.Select(n => n.GetOctahedron());
            var orderedOctahedrons = octahedrons.Select(o => (o, octahedrons.Count(o2 => o2.Intersect(o).IsValid)))
                                                .OrderByDescending(p => p.Item2)
                                                .ToArray();

            //const int firstFew = 450;
            //var initial = orderedOctahedrons.Select(p => p.o).Take(firstFew);
            //var init = initial.Aggregate(Octahedron.Blank(), (acc, o) => acc.Intersect(o));
            //var b = init.IsValid;

            //var result = SolveRec(orderedOctahedrons.Select(p => p.o).Skip(firstFew).ToArray(), Enumerable.Range(0, firstFew).ToArray(), init);
            var result = SolveRec(orderedOctahedrons.OrderByDescending(p => p.Item2).Select(p => p.o).ToArray(), new int[0], Octahedron.Blank());
            return (int)result.Distance;
        }

        private Dictionary<string, (int depth, long distance)> cache = new Dictionary<string, (int depth, long distance)>();
        public (bool IsValid, int Depth, long Distance) SolveRec(Octahedron[] all, int [] history, Octahedron current)
        {
            if (!current.IsValid)
            {
                return (false, 0, 0);
            }

            var key = string.Join(",", history.OrderBy(v => v));
            if (cache.TryGetValue(key, out var p))
            {
                return (true, p.depth, p.distance);
            }

            var newHistory = new List<int>(history);
            var hasValidSubState = false;
            var largestDepth = 0;
            var bestDistance = long.MaxValue;
            for(int i = 0; i < all.Length; i++)
            {
                if (!history.Contains(i))
                {
                    newHistory.Add(i);
                    var newOctohedron = current.Intersect(all[i]);
                    (var isValid, var depth, var distance) = SolveRec(all, newHistory.ToArray(), newOctohedron);

                    if (isValid)
                    {
                        hasValidSubState = true;
                        if (depth > largestDepth)
                        {
                            //largestDepth = depth;
                            //bestDistance = distance;

                            return (true, depth, distance);
                        }
                        //else if (depth == largestDepth)
                        //{
                        //    largestDepth = depth;
                        //    bestDistance = Math.Min(distance, bestDistance);
                        //}
                    }

                    newHistory.Remove(i);
                }
            }

            if (!hasValidSubState)
            {
                cache[key] = (history.Length, current.ShortestDistanceToOrigo);
                return (true, history.Length, current.ShortestDistanceToOrigo);
            }
            else
            {
                cache[key] = (largestDepth, bestDistance);
                return (true, largestDepth, bestDistance);
            }
        }
    }
}
