﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day23
{
    internal class Parser
    {
        private List<long> xs = new List<long>();
        private List<long> ys = new List<long>();
        private List<long> zs = new List<long>();
        public IEnumerable<Nanobot> ParseNanobots(IEnumerable<string> input)
        {
            var nanobots = new List<Nanobot>();
            foreach(var line in input)
            {
                var parts = line.Split('>');
                var coordinateParts = parts[0].Split('<')[1].Split(',');
                var x = long.Parse(coordinateParts[0]);
                var y = long.Parse(coordinateParts[1]);
                var z = long.Parse(coordinateParts[2]);

                xs.Add(x);
                ys.Add(y);
                zs.Add(z);

                var range = long.Parse(parts[1].Split('=')[1]);

                nanobots.Add(new Nanobot((x, y, z), range));
            }

            return nanobots;
        }

        public void Print()
        {
            System.Console.WriteLine($"Min x: {xs.Min()}, Max x: {xs.Max()}");
            System.Console.WriteLine($"Min y: {ys.Min()}, Max y: {ys.Max()}");
            System.Console.WriteLine($"Min z: {zs.Min()}, Max z: {zs.Max()}");

            var xd = xs.Max() - xs.Min();
            var yd = ys.Max() - ys.Min();
            var zd = zs.Max() - zs.Min();

            System.Console.WriteLine($"Search space: {xd} x {yd} x {zd} = {xd * yd * zd}");
        }
    }
}
