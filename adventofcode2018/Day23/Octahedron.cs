﻿using System;

namespace adventofcode2018.Day23
{
    class Octahedron : IEquatable<Octahedron>
    {
        private readonly (long Min, long Max) pXpYpZ; // => Min <=  x + y + z <= Max
        private readonly (long Min, long Max) pXnYpZ; // => Min <=  x - y + z <= Max
        private readonly (long Min, long Max) nXpYpZ; // => Min <= -x + y + z <= Max
        private readonly (long Min, long Max) nXnYpZ; // => Min <= -x - y + z <= Max
        public Octahedron(long x, long y, long z, long range)
        {
            pXpYpZ = (x + y + z - range, x + y + z + range);
            pXnYpZ = (x - y + z - range, x - y + z + range);
            nXpYpZ = (-x + y + z - range, -x + y + z + range);
            nXnYpZ = (-x - y + z - range, -x - y + z + range);
        }

        public Octahedron(Octahedron other)
        {
            pXpYpZ = other.pXpYpZ;
            pXnYpZ = other.pXnYpZ;
            nXpYpZ = other.nXpYpZ;
            nXnYpZ = other.nXnYpZ;
        }

        internal static Octahedron Blank()
        {
            return new Octahedron((long.MinValue, long.MaxValue), (long.MinValue, long.MaxValue), (long.MinValue, long.MaxValue), (long.MinValue, long.MaxValue));
        }

        private Octahedron((long Min, long Max) pXpYpZ,
                           (long Min, long Max) pXnYpZ,
                           (long Min, long Max) nXpYpZ,
                           (long Min, long Max) nXnYpZ)
        {
            this.pXpYpZ = pXpYpZ;
            this.pXnYpZ = pXnYpZ;
            this.nXpYpZ = nXpYpZ;
            this.nXnYpZ = nXnYpZ;
        }

        public bool IsValid => IsValidRange(pXpYpZ) && IsValidRange(pXnYpZ) && IsValidRange(nXpYpZ) && IsValidRange(nXnYpZ);
        public long ShortestDistanceToOrigo => pXpYpZ.Min;

        public Octahedron Intersect(Octahedron other)
        {
            return new Octahedron((Math.Max(pXpYpZ.Min, other.pXpYpZ.Min), Math.Min(pXpYpZ.Max, other.pXpYpZ.Max)),
                                  (Math.Max(pXnYpZ.Min, other.pXnYpZ.Min), Math.Min(pXnYpZ.Max, other.pXnYpZ.Max)),
                                  (Math.Max(nXpYpZ.Min, other.nXpYpZ.Min), Math.Min(nXpYpZ.Max, other.nXpYpZ.Max)),
                                  (Math.Max(nXnYpZ.Min, other.nXnYpZ.Min), Math.Min(nXnYpZ.Max, other.nXnYpZ.Max)));
        }

        private bool IsValidRange((long Min, long Max) range)
        {
            return range.Min <= range.Max;
        }

        public override string ToString()
        {
            return $"{pXpYpZ.Min} <=  x + y + z <= {pXpYpZ.Max}\n" +
                   $"{pXnYpZ.Min} <=  x - y + z <= {pXnYpZ.Max}\n" +
                   $"{nXpYpZ.Min} <= -x + y + z <= {nXpYpZ.Max}\n" +
                   $"{nXnYpZ.Min} <= -x - y + z <= {nXnYpZ.Max}\n";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Octahedron);
        }

        public bool Equals(Octahedron other)
        {
            return other != null &&
                   pXpYpZ.Equals(other.pXpYpZ) &&
                   pXnYpZ.Equals(other.pXnYpZ) &&
                   nXpYpZ.Equals(other.nXpYpZ) &&
                   nXnYpZ.Equals(other.nXnYpZ);
        }

        public override int GetHashCode()
        {
            int hashCode = -794264316;
            hashCode = hashCode * -1521134295 + pXpYpZ.GetHashCode();
            hashCode = hashCode * -1521134295 + pXnYpZ.GetHashCode();
            hashCode = hashCode * -1521134295 + nXpYpZ.GetHashCode();
            hashCode = hashCode * -1521134295 + nXnYpZ.GetHashCode();
            return hashCode;
        }
    }
}
