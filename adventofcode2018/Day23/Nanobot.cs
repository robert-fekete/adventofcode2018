﻿using System;

namespace adventofcode2018.Day23
{
    class Nanobot
    {
        private readonly long x;
        private readonly long y;
        private readonly long z;

        public Nanobot((long x, long y, long z) position, long range)
        {
            x = position.x;
            y = position.y;
            z = position.z;
            Range = range;
        }

        public long Range { get; }

        public bool IsInRange(Nanobot other)
        {
            var distance = Math.Abs(x - other.x) + Math.Abs(y - other.y) + Math.Abs(z - other.z);

            return distance <= Range;
        }

        public Octahedron GetOctahedron()
        {
            return new Octahedron(x, y, z, Range);
        }
    }
}
