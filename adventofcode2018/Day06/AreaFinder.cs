﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day06
{
    internal class AreaFinder
    {
        private readonly AreaMap map;
        private HashSet<int> extrema = new HashSet<int>();

        public AreaFinder(AreaMap map)
        {
            this.map = map;
            FindExtrema();
        }

        public int FindAreaIfBad()
        {
            var sizes = new Dictionary<int, int>();
            for (int y = map.MinY; y <= map.MaxY; y++)
            {
                for (int x = map.MinX; x <= map.MaxX; x++)
                {
                    var id = map.GetClosestCoordinate((x, y));
                    if (!sizes.ContainsKey(id))
                    {
                        sizes[id] = 0;
                    }

                    sizes[id]++;
                }
            }

            return sizes.Where(kvp => !extrema.Contains(kvp.Key))
                .Max(kvp => kvp.Value);
        }

        public int FindAreaIfGood(int limit)
        {
            int size = 0;
            for (int y = map.MinY; y <= map.MaxY; y++)
            {
                for (int x = map.MinX; x <= map.MaxX; x++)
                {
                    if(map.FindTotalDistance((x, y)) < limit)
                    {
                        size++;
                    }
                }
            }

            return size;
        }

        private void FindExtrema()
        {
            for (int x = map.MinX; x <= map.MaxX; x++)
            {
                var id1 = map.GetClosestCoordinate((x, map.MinY));
                extrema.Add(id1);

                var id2 = map.GetClosestCoordinate((x, map.MaxY));
                extrema.Add(id2);
            }

            for (int y = map.MinY; y <= map.MaxY; y++)
            {
                var id1 = map.GetClosestCoordinate((map.MinX, y));
                extrema.Add(id1);

                var id2 = map.GetClosestCoordinate((map.MaxX, y));
                extrema.Add(id2);
            }
        }
    }
}
