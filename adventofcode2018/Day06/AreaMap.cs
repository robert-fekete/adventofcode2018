﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day06
{
    internal class AreaMap
    {
        private readonly IEnumerable<(int, int)> coordinates;
        private readonly Func<(int, int), (int, int), int> distanceCalculator;

        public AreaMap(IEnumerable<(int, int)> coordinates, Func<(int, int), (int, int), int> distanceCalculator)
        {
            this.coordinates = coordinates;
            this.distanceCalculator = distanceCalculator;
            
            MinX = coordinates.Min(p => p.Item1);
            MaxX = coordinates.Max(p => p.Item1);
            
            MinY = coordinates.Min(p => p.Item2);
            MaxY = coordinates.Max(p => p.Item2);
        }

        public int MinX { get; }
        public int MaxX { get; }
        public int MinY { get; }
        public int MaxY { get; }
        
        public int GetClosestCoordinate((int, int) coordinate1)
        {
            double minDist = MaxY + MaxX;
            var idOfInterest = -1;
            int id = 0;
            int occurance = -1;

            foreach (var coordinate2 in coordinates)
            {
                var dist = distanceCalculator(coordinate1, coordinate2);
                if (dist == minDist)
                {
                    occurance += 1;
                }
                if (dist < minDist)
                {
                    minDist = dist;
                    idOfInterest = id;
                    occurance = 1;
                }

                id++;
            }

            if (occurance >= 2)
            {
                return -1;
            }
            else
            {
                return idOfInterest;
            }
        }

        public int FindTotalDistance((int, int) coordinate1)
        {
            return coordinates.Sum(coordinate2 => distanceCalculator(coordinate1, coordinate2));
        }
    }
}
