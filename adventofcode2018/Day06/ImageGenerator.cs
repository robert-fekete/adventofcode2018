﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace adventofcode2018.Day06
{
    internal class ImageGenerator
    {
        private IEnumerable<(int, int)> coordinates;
        
        public ImageGenerator(IEnumerable<(int, int)> coordinates3)
        {
            this.coordinates = coordinates3;
        }

        internal void SaveImage(string path, int margin)
        {
            var minX = coordinates.Min(p => p.Item1) - margin;
            var maxX = coordinates.Max(p => p.Item1) + margin;

            var minY = coordinates.Min(p => p.Item2) - margin;
            var maxY = coordinates.Max(p => p.Item2) + margin;

            var width = maxX - minX + 1;
            var height = maxY - minY + 1;

            var bitmap = new Bitmap(width, height);
            var colors = GetColors();

            for (int y = minY; y <= maxY; y++)
            {
                for(int x = minX; x <= maxX; x++)
                {
                    var id = GetClosestCoordinate(x, y, maxY + maxX);
                    var color = colors[id];
                    bitmap.SetPixel(x - minX, y - minY, color);
                }
            }

            foreach((var x, var y) in coordinates)
            {
                bitmap.SetPixel(x - minX, y - minY, Color.Red);
            }

            bitmap.Save(path);
        }

        private int GetClosestCoordinate(int x1, int y1, int init)
        {
            double minDist = init;
            var idOfInterest = -1;
            int id = 0;
            foreach((var x2, var y2) in coordinates)
            {
                var dist = CalculateDistance(x1, y1, x2, y2);
                if (dist < minDist)
                {
                    minDist = dist;
                    idOfInterest = id;
                }

                id++;
            }

            return idOfInterest;
        }

        private static double CalculateDistance(int x1, int y1, int x2, int y2)
        {
            //return Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            return Math.Abs(x2 - x1) + Math.Abs(y2 - y1);
        }

        private List<Color> GetColors()
        {
            var colorMax = 155;
            var randomShade = 255 - colorMax;
            var length = coordinates.Count();
            var perChannel = (int)Math.Ceiling(Math.Pow(length, 1.0/3.0));
            var diff = colorMax;
            if (length > 1)
            {
                diff = colorMax / (perChannel - 1);
            }

            var colors = new List<Color>();

            var random = new Random();
            for(int g = 0; g < perChannel; g++)
            {
                for (int r = 0; r < perChannel; r++)
                {
                    for (int b = 0; b < perChannel; b++)
                    {
                        int rand_r = (int)(random.Next(100) / 100.0 * randomShade);
                        int rand_g = (int)(random.Next(100) / 100.0 * randomShade);
                        int rand_b = (int)(random.Next(100) / 100.0 * randomShade);
                        colors.Add(Color.FromArgb(rand_r + r * diff, rand_g + g * diff, rand_b + b * diff));
                    }
                }
            }

            return colors;
        }
    }
}
