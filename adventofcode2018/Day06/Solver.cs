﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day06
{
    internal class Solver
    {
        public int First(IEnumerable<string> input)
        {
            var coordinates = ParseInput(input);
            var map = new AreaMap(coordinates, ManhattenDistance);
            var finder = new AreaFinder(map);

            return finder.FindAreaIfBad();
        }
        public int Second(IEnumerable<string> input, int limit)
        {
            var coordinates = ParseInput(input);
            var map = new AreaMap(coordinates, ManhattenDistance);
            var finder = new AreaFinder(map);

            return finder.FindAreaIfGood(limit);
        }

        private static int ManhattenDistance((int, int) coordinate1, (int, int) coordinate2)
        {
            (var x1, var y1) = coordinate1;
            (var x2, var y2) = coordinate2;

            return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
        }

        private IEnumerable<(int, int)> ParseInput(IEnumerable<string> input)
        {
            return input.Select(line =>
            {
                var coordinate = line.Split(',').
                    Select(part => part.Trim()).
                    Select(part => int.Parse(part));

                return (coordinate.First(), coordinate.Skip(1).First());
            });
        }
    }
}
