﻿using adventofcode2018.Day16;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day19
{
    internal class Solver
    {
        public long First(IEnumerable<string> input, int resultIndex)
        {

            var instructions = new InstructionCollection();
            var processor = new Processor(instructions);

            var program = ProgramParser.ParseCommandNameProgram(input);
            var ip = new InstructionPointer();
            var ipRegistry = ParseIpRegistryIndex(input);
            var registry = new Register(new long[] { 0, 0, 0, 0, 0, 0 });

            processor.Execute(program,
                              ip,
                              registry,
                              () => registry.SetValue(ipRegistry, ip.Value),
                              () => ip.Value = registry.GetValue(ipRegistry));

            return registry.GetValue(resultIndex);
        }

        public long Second(IEnumerable<string> input)
        {
            var instructions = new InstructionCollection();
            var processor = new Processor(instructions);

            var program = ProgramParser.ParseCommandNameProgram(input);
            var ip = new InstructionPointer();
            var ipRegistry = ParseIpRegistryIndex(input);
            var registry = new Register(new long[] { 1, 0, 0, 0, 0, 0 });

            // Skipping the last jump of the program, so we return after we got the
            // magic number. This way we avoid the whole factorization logic and
            // we can just do it in C#
            processor.Execute(program.Take(program.Count - 1),
                              ip,
                              registry,
                              () => registry.SetValue(ipRegistry, ip.Value),
                              () => ip.Value = registry.GetValue(ipRegistry));
            var magicNumber = Enumerable.Range(0, 6).Max(i => registry.GetValue(i));

            var factors = GetFactors(magicNumber);
            var result = factors.Sum();

            return result;
        }

        private int ParseIpRegistryIndex(IEnumerable<string> input)
        {
            var parts = input.First().Split(' ');

            return int.Parse(parts[1]);
        }

        private IEnumerable<long> GetFactors(long number)
        {
            var factors = new List<long>();
            for(long i = 1; i <= number; i++)
            {
                if (number % i == 0)
                {
                    factors.Add(i);
                }
            }

            return factors;
        }
    }
}
