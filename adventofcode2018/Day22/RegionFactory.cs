﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day22
{
    internal class RegionFactory
    {
        public int NumberOfTypes => 3;
        
        public Region CreateRegion(long erosionLevel)
        {
            var type = (int) erosionLevel % NumberOfTypes;
            var rejectedTools = GetRejectedTools(type);

            return new Region(type, rejectedTools);
        }

        private IEnumerable<Tools> GetRejectedTools(int type)
        {
            switch (type)
            {
                case 0: return new[] { Tools.Neither };
                case 1: return new[] { Tools.Torch };
                case 2: return new[] { Tools.ClimbingGear };
                default:
                    throw new InvalidOperationException("Invalid region type for tools");
            }
        }
    }
}
