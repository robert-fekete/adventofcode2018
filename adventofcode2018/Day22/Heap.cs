﻿using System;

namespace adventofcode2018.Day22
{
    internal class Heap<T>
    {
        private readonly Func<T, int> weight;
        private T[] elements;
        private int currentSize;

        public Heap(Func<T, int> weight)
        {
            elements = new T[10];
            this.weight = weight;
        }

        private int GetLeftChildIndex(int elementIndex) => 2 * elementIndex + 1;
        private int GetRightChildIndex(int elementIndex) => 2 * elementIndex + 2;
        private int GetParentIndex(int elementIndex) => (elementIndex - 1) / 2;

        private bool HasLeftChild(int elementIndex) => GetLeftChildIndex(elementIndex) < currentSize;
        private bool HasRightChild(int elementIndex) => GetRightChildIndex(elementIndex) < currentSize;
        private bool IsRoot(int elementIndex) => elementIndex == 0;

        private T GetLeftChild(int elementIndex) => elements[GetLeftChildIndex(elementIndex)];
        private T GetRightChild(int elementIndex) => elements[GetRightChildIndex(elementIndex)];
        private T GetParent(int elementIndex) => elements[GetParentIndex(elementIndex)];

        private void Swap(int firstIndex, int secondIndex)
        {
            var temp = elements[firstIndex];
            elements[firstIndex] = elements[secondIndex];
            elements[secondIndex] = temp;
        }

        public bool IsEmpty()
        {
            return currentSize == 0;
        }

        public T Peek()
        {
            if (currentSize == 0)
                throw new IndexOutOfRangeException();

            return elements[0];
        }

        public T Pop()
        {
            if (currentSize == 0)
            {
                throw new IndexOutOfRangeException();
            }

            var result = elements[0];
            elements[0] = elements[currentSize - 1];
            currentSize--;

            ReCalculateDown();

            return result;
        }

        public void Add(T element)
        {
            if (currentSize == elements.Length)
            {
                var newArray = new T[elements.Length * 2];
                for(int i = 0; i < elements.Length; i++)
                {
                    newArray[i] = elements[i];
                }
                elements = newArray;
            }

            elements[currentSize] = element;
            currentSize++;

            ReCalculateUp();
        }

        private void ReCalculateDown()
        {
            int index = 0;
            while (HasLeftChild(index))
            {
                var smallerIndex = GetLeftChildIndex(index);
                if (HasRightChild(index) && weight(GetRightChild(index)) < weight(GetLeftChild(index)))
                {
                    smallerIndex = GetRightChildIndex(index);
                }

                if (weight(elements[smallerIndex]) >= weight(elements[index]))
                {
                    break;
                }

                Swap(smallerIndex, index);
                index = smallerIndex;
            }
        }

        private void ReCalculateUp()
        {
            var index = currentSize - 1;
            while (!IsRoot(index) && weight(elements[index]) < weight(GetParent(index)))
            {
                var parentIndex = GetParentIndex(index);
                Swap(parentIndex, index);
                index = parentIndex;
            }
        }
    }
}
