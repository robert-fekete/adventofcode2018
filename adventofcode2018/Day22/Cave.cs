﻿using System.Collections.Generic;

namespace adventofcode2018.Day22
{
    internal class Cave
    {
        private readonly RegionFactory factory = new RegionFactory();
        private readonly Dictionary<(int, int), long> cache = new Dictionary<(int, int), long>();

        private readonly (int X, int Y) entrance;
        private readonly (int X, int Y) target;
        private readonly long depth;
        private readonly long mod;

        public Cave(long depth, (int X, int Y) entrance, (int X, int Y) target, long mod)
        {
            this.depth = depth;
            this.entrance = entrance;
            this.target = target;
            this.mod = mod;
        }

        public Region GetRegion(int x, int y)
        {
            var erosionLevel = GetErosionLevel(x, y);

            return factory.CreateRegion(erosionLevel);
        }

        private long GetErosionLevel(int x, int y)
        {
            if (cache.ContainsKey((x, y)))
            {
                return cache[(x, y)];
            }

            cache[(x, y)] = GetErosionLevelCalc(x, y);
            return cache[(x, y)];
        }

        private long GetErosionLevelCalc(int x, int y)
        {
            if (x == target.X && y == target.Y)
            {
                return depth % mod;
            }
            if (x == entrance.X && y == entrance.Y)
            {
                return depth % mod;
            }

            if (x == 0)
            {
                return (y * 48271 + depth) % mod;
            }
            if (y == 0)
            {
                return (x * 16807 + depth) % mod;
            }

            var geoIndex = GetErosionLevel(x - 1, y) * GetErosionLevel(x, y - 1);
            return (geoIndex + depth) % mod;
        }
    }
}
