﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day22
{
    internal class Solver
    {
        // 5637
        internal long First(IEnumerable<string> input)
        {
            var mod = 20183;
            var depth = int.Parse(input.First().Split(':')[1].Trim());
            var targetCoordinates = input.Skip(1).First().Split(':')[1].Trim().Split(',');
            (var targetX, var targetY) = (int.Parse(targetCoordinates[0]), int.Parse(targetCoordinates[1]));

            var cave = new Cave(depth, (0, 0), (targetX, targetY), mod);

            long risk = 0;
            for (int y = 0; y < targetY + 1; y++)
            {
                for(int x = 0; x < targetX + 1; x++)
                {
                    var region = cave.GetRegion(x, y);
                    risk += region.RiskLevel;
                }
            }

            return risk;
        }

        internal long Second(IEnumerable<string> input)
        {
            var mod = 20183;
            var depth = int.Parse(input.First().Split(':')[1].Trim());
            var targetCoordinates = input.Skip(1).First().Split(':')[1].Trim().Split(',');
            (var targetX, var targetY) = (int.Parse(targetCoordinates[0]), int.Parse(targetCoordinates[1]));

            var cave = new Cave(depth, (0, 0), (targetX, targetY), mod);

            var time = Traverse(cave, (0, 0), (targetX, targetY));

            return time;
        }

        // A*
        private int Traverse(Cave cave, (int, int) source, (int, int) target)
        {
            var backlog = new Heap<((int, int), ToolSet, int, int Score)>(x => x.Score);
            var visited = new HashSet<((int, int), ToolSet)>();

            var bestTimes = new List<int>();
            backlog.Add((source, ToolSet.StartingTool, 0, CalculateScore(source, target, ToolSet.StartingTool)));
            while (!backlog.IsEmpty())
            {
                (var current, var toolSet, var time, var _) = backlog.Pop();

                if (visited.Contains((current, toolSet)))
                {
                    continue;
                }
                visited.Add((current, toolSet));

                if (toolSet.HasTool(Tools.Torch) && current == target)
                {
                    return time;
                }

                foreach (var next in GetNext(current, toolSet, cave))
                {
                    var newTime = time + 1;
                    backlog.Add((next, toolSet, newTime, newTime + CalculateScore(next, target, toolSet)));
                }

                var region = cave.GetRegion(current.Item1, current.Item2);
                foreach (var nextToolSet in toolSet.GetVariations())
                {
                    if (region.CanEnter(nextToolSet))
                    {
                        var newTime = time + 7;
                        backlog.Add((current, nextToolSet, newTime, newTime + CalculateScore(current, target, nextToolSet)));
                    }
                }
            }

            throw new Exception("Couldn't find target");
        }

        private int CalculateScore((int, int) source, (int, int) target, ToolSet startingTool)
        {
            var distance = Math.Abs(target.Item1 - source.Item1) + Math.Abs(target.Item2 - source.Item2);
            var toolScore = startingTool.HasTool(Tools.Torch) ? 0 : 7;

            return distance + toolScore;
        }

        private IEnumerable<(int, int)> GetNext((int, int) current, ToolSet toolSet, Cave cave)
        {
            var offsets = new[]
            {
                (1, 0),
                (0, 1),
                (-1, 0),
                (0, -1)
            };

            var nexts = new List<(int, int)>();
            foreach ((var dx, var dy) in offsets)
            {
                var x = current.Item1 + dx;
                var y = current.Item2 + dy;

                if (x < 0 || y < 0)
                {
                    continue;
                }

                var region = cave.GetRegion(x, y);
                if (region.CanEnter(toolSet))
                {
                    nexts.Add((x, y));
                }
            }

            return nexts;
        }
    }
}