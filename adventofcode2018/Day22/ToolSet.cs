﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace adventofcode2018.Day22
{
    [DebuggerDisplay("{tool}")]
    internal class ToolSet
    {
        private readonly Tools tool;

        private ToolSet(Tools tool)
        {
            this.tool = tool;
        }

        public static ToolSet StartingTool => new ToolSet(Tools.Torch);

        internal IEnumerable<ToolSet> GetVariations()
        {
            var allVariations = new[]
            {
                Tools.Neither,
                Tools.Torch,
                Tools.ClimbingGear,
            };

            var differentVariations = allVariations.Where(v => v != tool);
            return differentVariations.Select(v => new ToolSet(v));
        }

        internal bool HasTool(Tools tool)
        {
            return this.tool == tool;
        }

        public override bool Equals(object obj)
        {
            return obj is ToolSet set &&
                   tool == set.tool;
        }

        public override int GetHashCode()
        {
            return 347762911 + tool.GetHashCode();
        }
    }
}
