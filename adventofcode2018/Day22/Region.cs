﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day22
{
    internal class Region
    {
        private readonly int type;
        private readonly IEnumerable<Tools> rejectedTools;

        public Region(int type, IEnumerable<Tools> rejectedTools)
        {
            this.type = type;
            this.rejectedTools = rejectedTools;
        }

        public long RiskLevel => type;

        internal bool CanEnter(ToolSet toolSet)
        {
            return !rejectedTools.Any(t => toolSet.HasTool(t));
        }
    }
}
