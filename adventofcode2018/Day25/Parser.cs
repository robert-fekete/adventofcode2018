﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day25
{
    internal class Parser
    {
        internal IEnumerable<(int, int, int, int)> ParseInput(IEnumerable<string> input)
        {
            return input.Select(l =>
            {
                var parts = l.Split(',');
                return (int.Parse(parts[0]), int.Parse(parts[1]), int.Parse(parts[2]), int.Parse(parts[3]));
            });
        }
    }
}