﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day25
{
    class BFS
    {
        private readonly Graph graph;

        public BFS(Graph graph)
        {
            this.graph = graph;
        }

        public IEnumerable<(int, int, int, int)> FindConstellation((int, int, int, int) node)
        {
            var backlog = new Queue<(int, int, int, int)>();

            var visited = new HashSet<(int, int, int, int)>();

            backlog.Enqueue(node);

            while (backlog.Any())
            {
                var current = backlog.Dequeue();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                foreach(var next in graph.GetNeighbours(current))
                {
                    backlog.Enqueue(next);
                }
            }

            return visited;
        }
    }
}
