﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day25
{
    class Solver
    {
        public int First(IEnumerable<string> input)
        {
            var parser = new Parser();
            var nodes = parser.ParseInput(input);

            var graph = new Graph(nodes, 3);
            var bfs = new BFS(graph);

            var visited = new HashSet<(int, int, int, int)>();

            var constillations = 0;
            foreach(var node in graph.Nodes)
            {
                if (visited.Contains(node))
                {
                    continue;
                }
                constillations++;

                foreach(var next in bfs.FindConstellation(node))
                {
                    visited.Add(next);
                }
            }

            return constillations;
        }
    }
}
