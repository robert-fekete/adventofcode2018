﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day25
{
    class Graph
    {
        private readonly int limit;

        public Graph(IEnumerable<(int, int, int, int)> nodes, int limit)
        {
            Nodes = nodes;
            this.limit = limit;
        }

        public IEnumerable<(int, int, int, int)> Nodes { get; }

        public IEnumerable<(int, int, int, int)> GetNeighbours((int, int, int, int) node)
        {
            return Nodes.Where(n => AreNeighours(n, node));
        }

        private bool AreNeighours((int, int, int, int) n1, (int, int, int, int) n2)
        {
            return Math.Abs(n1.Item1 - n2.Item1) + Math.Abs(n1.Item2 - n2.Item2) + Math.Abs(n1.Item3 - n2.Item3) + Math.Abs(n1.Item4 - n2.Item4) <= limit;
        }
    }
}
