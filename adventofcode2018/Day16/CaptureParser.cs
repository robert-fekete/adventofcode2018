﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day16
{
    internal class CaptureParser
    {
        internal IEnumerable<Capture> GetCaptures(IEnumerable<string> input)
        {
            var result = new List<Capture>();

            var lines = input.ToArray();
            var iter = 0;
            while (iter < lines.Length)
            {
                if (lines[iter].Length == 0)
                {
                    break;
                }

                var before = GetRegister(lines[iter]);
                var numbers = lines[iter + 1].Split(' ').Select(p => Int32.Parse(p)).ToArray();
                var after = GetRegister(lines[iter + 2]);

                var capture = new Capture(before, after, numbers[0], numbers[1], numbers[2], numbers[3]);
                result.Add(capture);

                iter += 4;
            }

            return result;
        }

        private Register GetRegister(string line)
        {
            var numbers = line.Split(':')[1];
            var values = numbers.Split(',').Select(p => p.Trim(' ', '[', ']')).Select(p => Int32.Parse(p)).ToArray();

            return new Register(values[0], values[1], values[2], values[3]);
        }
    }
}