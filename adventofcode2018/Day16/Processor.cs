﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day16
{
    internal class Processor
    {
        private readonly InstructionCollection instructions;

        public Processor(InstructionCollection instructions)
        {
            this.instructions = instructions;
        }

        public Register Execute(IEnumerable<(string, int, int, int)> program)
        {
            return Execute(program, new InstructionPointer(), new Register(0, 0, 0, 0), null, null);
        }

        public Register Execute(IEnumerable<(string, int, int, int)> input,
                                 InstructionPointer ip,
                                 Register registry,
                                 Action preInstruction,
                                 Action postInstruction)
        {
            var program = input.ToArray();
            while (ip.Value < program.Length && ip.Value >= 0)
            {
                (var opcode, int a, int b, int c) = program[ip.Value];

                preInstruction?.Invoke();
                instructions[opcode].Execute(registry, a, b, c);
                postInstruction?.Invoke();

                ip.Value++;
            }

            return registry;
        }
    }
}
