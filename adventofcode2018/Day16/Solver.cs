﻿using System.Collections.Generic;

namespace adventofcode2018.Day16
{
    internal class Solver
    {
        private const int THRESHOLD = 3;

        public int First(IEnumerable<string> input)
        {
            var parser = new CaptureParser();
            var captures = parser.GetCaptures(input);
            var instructions = new InstructionCollection();

            var result = instructions.CountInstructionsAboveMatchTreshold(captures, THRESHOLD);

            return result;
        }

        internal long Second(IEnumerable<string> input)
        {
            var parser = new CaptureParser();
            var captures = parser.GetCaptures(input);
            var instructions = new InstructionCollection();
            var mapping = instructions.GetInstructionMapping(captures);

            var program = ProgramParser.ParseOpcodeProgram(input, mapping);
            var processor = new Processor(instructions);
            var registry = processor.Execute(program);

            return registry.GetValue(0);
        }
    }
}
