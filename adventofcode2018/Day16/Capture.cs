﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day16
{
    internal class Capture
    {
        private readonly Register before;
        private readonly Register after;
        private readonly int a;
        private readonly int b;
        private readonly int c;

        public Capture(Register before, Register after, int opcode, int a, int b, int c)
        {
            this.before = before;
            this.after = after;
            Opcode = opcode;
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public int Opcode { get; }

        public IEnumerable<IInstruction> GetMatchingInstructions(IEnumerable<IInstruction> instructions)
        {
            var matcing = instructions.Where(IsMatch);
            return matcing;
        }

        public bool IsMatch(IInstruction instruction)
        {
            var temp = before.Clone();
            instruction.Execute(temp, a, b, c);
            return temp == after;
        }
    }
}
