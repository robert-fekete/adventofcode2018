﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day16
{
#pragma warning disable CS0660 // Type defines operator == or operator != but does not override Object.Equals(object o)
#pragma warning disable CS0661 // Type defines operator == or operator != but does not override Object.GetHashCode()
    internal class Register
#pragma warning restore CS0661 // Type defines operator == or operator != but does not override Object.GetHashCode()
#pragma warning restore CS0660 // Type defines operator == or operator != but does not override Object.Equals(object o)
    {
        private readonly long[] values;

        public Register(params long[] values)
        {
            this.values = values.ToArray();
        }

        internal Register Clone()
        {
            return new Register(values);
        }

        public int Length => values.Length;

        public long GetValue(int index)
        {
            if (index < 0 || index >= Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index), $"Value {index}");
            }

            return values[index];
        }

        public void SetValue(int index, long value)
        {
            if (index < 0 || index >= Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index), $"Value {index}");
            }

            values[index] = value;
        }

        public static bool operator ==(Register a, Register b)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (a.GetValue(i) != b.values[i])
                {
                    return false;
                }
            }

            return true;
        }

        public static bool operator !=(Register a, Register b)
        {
            return !(a == b);
        }
    }
}