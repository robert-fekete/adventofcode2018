﻿namespace adventofcode2018.Day16
{
    internal interface IInstruction
    {
        void Execute(Register register, int a, int b, int c);
    }
}
