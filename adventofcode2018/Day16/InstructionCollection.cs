﻿using System;
using System.Collections.Generic;
using System.Linq;
using adventofcode2018.Day16.Instructions;

namespace adventofcode2018.Day16
{
    internal class InstructionCollection
    {
        private readonly Dictionary<string, IInstruction> instructions = new Dictionary<string, IInstruction>()
        {
            { "addr", new AddRegister() },
            { "addi", new AddImmediate() },
            { "mulr", new MultiplyRegister() },
            { "muli", new MultiplyImmediate() },
            { "banr", new BitwiseAndRegister() },
            { "bani", new BitwiseAndImmediate() },
            { "borr", new BitwiseOrRegister() },
            { "bori", new BitwiseOrImmediate() },
            { "setr", new AssignmentRegister() },
            { "seti", new AssignmentImmediate() },
            { "gtir", new GreaterThanImmediateRegister() },
            { "gtri", new GreaterThanRegisterImmediate() },
            { "gtrr", new GreaterThanRegisterRegister() },
            { "eqir", new EqualityImmediateRegister() },
            { "eqri", new EqualityRegisterImmediate() },
            { "eqrr", new EqualityRegisterRegister() }
        };

        public IInstruction this[string key]
        {
            get => instructions[key];
        }

        public int CountInstructionsAboveMatchTreshold(IEnumerable<Capture> captures, int threshold)
        {
            return captures.Where(c => c.GetMatchingInstructions(instructions.Values).Count() >= threshold).Count();
        }

        public Dictionary<int, string> GetInstructionMapping(IEnumerable<Capture> captures)
        {
            var mapping = GetInitialMapping();

            // Limiting based on captures
            foreach (var capture in captures)
            {
                var matchingSet = new List<IInstruction>();
                foreach (var instruction in mapping[capture.Opcode])
                {
                    if (capture.IsMatch(instruction))
                    {
                        matchingSet.Add(instruction);
                    }
                }
                mapping[capture.Opcode] = matchingSet;
            }

            // Fixing instructions based on other existing pairing
            while(mapping.Any(kvp => kvp.Value.Count > 1))
            {
                foreach (var current in mapping)
                {
                    if (current.Value.Count == 1)
                    {
                        var instruction = current.Value[0];
                        foreach(var other in mapping)
                        {
                            if (other.Key == current.Key)
                            {
                                continue;
                            }
                            if (other.Value.Contains(instruction))
                            {
                                other.Value.Remove(instruction);
                            }
                        }
                    }
                }
            }

            var result = new Dictionary<int, string>();
            foreach(var kvp in mapping)
            {
                result[kvp.Key] = instructions.First(kvp2 => kvp2.Value == kvp.Value.First()).Key;
            }
            return result;
        }

        private Dictionary<int, List<IInstruction>> GetInitialMapping()
        {
            var mapping = new Dictionary<int, List<IInstruction>>();
            for (int i = 0; i < instructions.Count; i++)
            {
                mapping[i] = instructions.Values.ToList();
            }

            return mapping;
        }
    }
}
