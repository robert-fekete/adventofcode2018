﻿namespace adventofcode2018.Day16.Instructions
{
    internal class AddImmediate : IInstruction
    {
        public void Execute(Register register, int a, int b, int c)
        {
            var result = register.GetValue(a) + b;
            register.SetValue(c, result);
        }
    }
}
