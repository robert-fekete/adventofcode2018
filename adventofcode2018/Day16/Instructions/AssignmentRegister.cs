﻿namespace adventofcode2018.Day16.Instructions
{
    internal class AssignmentRegister : IInstruction
    {
        public void Execute(Register register, int a, int b, int c)
        {
            register.SetValue(c, register.GetValue(a));
        }
    }
}