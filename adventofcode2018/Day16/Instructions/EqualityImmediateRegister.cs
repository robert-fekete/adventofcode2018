﻿namespace adventofcode2018.Day16.Instructions
{
    internal class EqualityImmediateRegister : IInstruction
    {
        public void Execute(Register register, int a, int b, int c)
        {
            var result = a == register.GetValue(b) ? 1 : 0;
            register.SetValue(c, result);
        }
    }
}