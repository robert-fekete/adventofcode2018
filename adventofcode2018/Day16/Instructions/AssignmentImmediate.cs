﻿namespace adventofcode2018.Day16.Instructions
{
    internal class AssignmentImmediate : IInstruction
    {
        public void Execute(Register register, int a, int b, int c)
        {
            register.SetValue(c, a);
        }
    }
}
