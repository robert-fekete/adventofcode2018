﻿namespace adventofcode2018.Day16.Instructions
{
    internal class GreaterThanRegisterImmediate : IInstruction
    {
        public void Execute(Register register, int a, int b, int c)
        {
            var result = register.GetValue(a) > b ? 1 : 0;
            register.SetValue(c, result);
        }
    }
}