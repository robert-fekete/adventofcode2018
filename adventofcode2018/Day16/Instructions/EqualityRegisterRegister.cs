﻿namespace adventofcode2018.Day16.Instructions
{
    internal class EqualityRegisterRegister : IInstruction
    {
        public void Execute(Register register, int a, int b, int c)
        {
            var result = register.GetValue(a) == register.GetValue(b) ? 1 : 0;
            register.SetValue(c, result);
        }
    }
}