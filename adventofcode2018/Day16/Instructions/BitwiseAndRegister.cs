﻿namespace adventofcode2018.Day16.Instructions
{
    internal class BitwiseAndRegister : IInstruction
    {
        public void Execute(Register register, int a, int b, int c)
        {
            var result = register.GetValue(a) & register.GetValue(b);
            register.SetValue(c, result);
        }
    }
}
