﻿namespace adventofcode2018.Day16.Instructions
{
    internal class BitwiseOrImmediate : IInstruction
    {
        public void Execute(Register register, int a, int b, int c)
        {
            var result = register.GetValue(a) | (long)b;
            register.SetValue(c, result);
        }
    }
}
