﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day16
{
    internal class ProgramParser
    {
        public static List<(string, int, int, int)> ParseOpcodeProgram(IEnumerable<string> input, Dictionary<int, string> mapping)
        {
            var lines = input.ToList();
            var startIndex = FindStartingIndex(lines);

            return ParseProgram(lines, startIndex, o => mapping[int.Parse(o)]);
        }

        private static int FindStartingIndex(List<string> lines)
        {
            var index = 0;
            while (lines[index].StartsWith("Before"))
            {
                index += 4;
            }

            index++;
            index++;
            return index;
        }

        public static List<(string, int, int, int)> ParseCommandNameProgram(IEnumerable<string> input)
        {
            var lines = input.ToList();
            return ParseProgram(lines, 1, o => o);
        }

        private static List<(string, int, int, int)> ParseProgram(List<string> lines, int startIndex, Func<string, string> getOpcode)
        {
            var program = new List<(string, int, int, int)>();
            for (var i = startIndex; i < lines.Count; i++)
            {
                var parts = lines[i].Split(' ');
                var opcode = getOpcode(parts[0]);
                var a = int.Parse(parts[1]);
                var b = int.Parse(parts[2]);
                var c = int.Parse(parts[3]);

                program.Add((opcode, a, b, c));
            }

            return program;
        }
    }
}
