﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day20
{
    internal class Solver
    {
        public long First(string input)
        {
            input = input.Substring(1, input.Length - 2);

            var explorer = new Explorer();
            var graph = explorer.GetGraph(input);
            var bfs = new BfsTraversal(graph);

            var longest = 0;
            bfs.Traverse(depth =>
            {
                if (depth > longest)
                {
                    longest = depth;
                }
            });

            return longest;
        }

        public long Second(string input)
        {
            input = input.Substring(1, input.Length - 2);

            var explorer = new Explorer();
            var graph = explorer.GetGraph(input);
            var bfs = new BfsTraversal(graph);

            var rooms = 0;
            bfs.Traverse(depth =>
            {
                if (depth >= 1000)
                {
                    rooms++;
                }
            });

            return rooms;
        }
    }
}
