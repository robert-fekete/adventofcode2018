﻿using System;
using System.Collections.Generic;

namespace adventofcode2018.Day20
{
    public class Graph
    {
        private readonly Dictionary<(int, int), IEnumerable<(int, int)>> edges;

        public Graph((int, int) root, Dictionary<(int, int), IEnumerable<(int, int)>> edges)
        {
            Root = root;
            this.edges = edges;
        }

        public (int, int) Root { get; }

        internal IEnumerable<(int, int)> GetNeighbours((int, int) current)
        {
            return edges[current];
        }
    }
}