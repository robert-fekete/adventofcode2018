﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day20
{
    internal class GraphBuilder
    {
        private readonly List<(int, int)> nodes = new List<(int, int)>();
        private readonly Dictionary<(int, int), List<(int, int)>> edges = new Dictionary<(int, int), List<(int, int)>>();
        private (int, int) root;

        internal Graph Create()
        {
            return new Graph(root, edges.ToDictionary(kvp => kvp.Key, kvp => kvp.Value as IEnumerable<(int, int)>));
        }

        internal void AddNode((int, int) node)
        {
            nodes.Add(node);
            edges[node] = new List<(int, int)>();
        }

        internal void AddEdge((int, int) node, (int, int) otherNode)
        {
            edges[node].Add(otherNode);
            edges[otherNode].Add(node);
        }

        internal void SetRoot((int, int) node)
        {
            root = node;
        }
    }
}