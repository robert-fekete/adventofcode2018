﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adventofcode2018.Day20
{
    internal class Explorer
    {
        public Graph GetGraph(string input)
        {
            var builder = new GraphBuilder();
            builder.AddNode((0, 0));
            builder.SetRoot((0, 0));

            ExploreBranchRecursive(input, 0, (0, 0), builder);

            return builder.Create();
        }

        private void ExploreBranchRecursive(string input, int position, (int, int) coordinates, GraphBuilder builder)
        {
            (int X, int Y) currentCoordinates = coordinates;
            while (position < input.Length)
            {
                if (input[position] == '(')
                {
                    var endPosition = GetBranchEndPosition(input, position);
                    var branchLength = endPosition - position - 1;
                    var branch = input.Substring(position + 1, branchLength - 1);
                    ExploreBranchRecursive(branch, 0, currentCoordinates, builder);
                    position = endPosition;
                }
                else if (input[position] == '|')
                {
                    currentCoordinates = coordinates;
                    position++;
                }
                else if (input[position] == ')')
                {
                    throw new InvalidOperationException("Shouldn't see ')'");
                }
                else if (input[position] == 'N')
                {
                    var nextCoordinate = (currentCoordinates.X, currentCoordinates.Y - 1);
                    builder.AddNode(nextCoordinate);
                    builder.AddEdge(currentCoordinates, nextCoordinate);
                    currentCoordinates = nextCoordinate;
                    position++;
                }
                else if (input[position] == 'S')
                {
                    var nextCoordinate = (currentCoordinates.X, currentCoordinates.Y + 1);
                    builder.AddNode(nextCoordinate);
                    builder.AddEdge(currentCoordinates, nextCoordinate);
                    currentCoordinates = nextCoordinate;
                    position++;
                }
                else if (input[position] == 'W')
                {
                    var nextCoordinate = (currentCoordinates.X - 1, currentCoordinates.Y);
                    builder.AddNode(nextCoordinate);
                    builder.AddEdge(currentCoordinates, nextCoordinate);
                    currentCoordinates = nextCoordinate;
                    position++;
                }
                else if (input[position] == 'E')
                {
                    var nextCoordinates = (currentCoordinates.X + 1, currentCoordinates.Y);
                    builder.AddNode(nextCoordinates);
                    builder.AddEdge(currentCoordinates, nextCoordinates);
                    currentCoordinates = nextCoordinates;
                    position++;
                }
                else
                {
                    throw new InvalidOperationException("Invalid character");
                }
            }
        }

        private int GetBranchEndPosition(string input, int position)
        {
            var level = 1;
            position++;
            while (position < input.Length && level > 0)
            {
                if (input[position] == '(')
                {
                    level++;
                }
                else if (input[position] == ')')
                {
                    level--;
                }
                position++;
            }

            return position;
        }
    }
}
