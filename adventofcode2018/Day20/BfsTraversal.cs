﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day20
{
    internal class BfsTraversal
    {
        private readonly Graph graph;

        public BfsTraversal(Graph graph)
        {
            this.graph = graph;
        }

        public void Traverse(Action<int> callback)
        {
            var backlog = new Queue<((int, int), int)>();
            var visited = new HashSet<(int, int)>();

            backlog.Enqueue((graph.Root, 0));
            while (backlog.Any())
            {
                (var current, var depth) = backlog.Dequeue();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                callback(depth);

                foreach(var next in graph.GetNeighbours(current))
                {
                    backlog.Enqueue((next, depth + 1));
                }
            }
        }
    }
}
