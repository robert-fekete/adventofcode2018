﻿namespace adventofcode2018.Day10
{
    internal class LightPoint
    {
        private readonly int velocityX;
        private readonly int velocityY;

        public int X { get; private set; }
        public int Y { get; private set; }

        public LightPoint(int x, int y, int vx, int vy)
        {
            X = x;
            Y = y;
            velocityX = vx;
            velocityY = vy;
        }

        public void Step()
        {
            X += velocityX;
            Y += velocityY;
        }
    }
}
