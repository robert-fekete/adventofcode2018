﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace adventofcode2018.Day10
{
    internal class Message
    {
        private readonly IEnumerable<LightPoint> lights;

        public Message(IEnumerable<LightPoint> lights)
        {
            this.lights = lights;
        }
        public int Iterations { get; private set; } = 0;

        private int MinX => lights.Min(light => light.X);
        private int MaxX => lights.Max(light => light.X);
        private int MinY => lights.Min(light => light.Y);
        private int MaxY => lights.Max(light => light.Y);

        public void IterateUntil(int heightDifference)
        {
            while(MaxY - MinY > heightDifference)
            {
                Iterations++;
                StepLights();
            }
        }

        private void StepLights()
        {
            foreach(var light in lights)
            {
                light.Step();
            }
        }

        public Bitmap Print()
        {
            var width = MaxX - MinX + 1;
            var height = MaxY - MinY + 1;
            var bitmap = new Bitmap(width, height);

            foreach (var light in lights)
            {
                var x = light.X - MinX;
                var y = light.Y - MinY;
                bitmap.SetPixel(x, y, Color.Red);
            }

            return bitmap;
        }
    }
}
