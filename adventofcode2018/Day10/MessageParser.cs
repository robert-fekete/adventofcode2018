﻿using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day10
{
    internal class MessageParser
    {
        private const int X_INDEX = 1;
        private const int Y_INDEX = 2;
        private const int VELOCITY_X_INDEX = 4;
        private const int VELOCITY_Y_INDEX = 5;

        public Message Parse(IEnumerable<string> input)
        {
            var result = new List<LightPoint>();
            foreach (var line in input)
            {
                var parts = line.Split('<', '>', ',');
                var x = int.Parse(parts[X_INDEX].Trim(',', ' '));
                var y = int.Parse(parts[Y_INDEX].Trim('>', ' '));
                var vx = int.Parse(parts[VELOCITY_X_INDEX].Trim(',', ' '));
                var vy = int.Parse(parts[VELOCITY_Y_INDEX].Trim('>', ' '));

                result.Add(new LightPoint(x, y, vx, vy));
            }

            return new Message(result);
        }
    }
}
