﻿using System.Collections.Generic;
using System.Drawing;

namespace adventofcode2018.Day10
{
    internal class Solver
    {
        public Bitmap First(IEnumerable<string> input, int heightDifference)
        {
            var parser = new MessageParser();
            var message = parser.Parse(input);

            message.IterateUntil(heightDifference);

            return message.Print();
        }
        public int Second(IEnumerable<string> input, int heightDifference)
        {
            var parser = new MessageParser();
            var message = parser.Parse(input);

            message.IterateUntil(heightDifference);

            return message.Iterations;
        }
    }
}
