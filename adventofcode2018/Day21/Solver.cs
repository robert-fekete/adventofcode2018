﻿using adventofcode2018.Day16;
using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode2018.Day21
{
    internal class Solver
    {
        public long First(IEnumerable<string> input)
        {
            //var startingValue = 13970209;   // Acquired by analysing the source code

            long firstValue = -1;
            Simulate(r2 =>
            {
                firstValue = r2;
                return true;
            });

            TestStartValue(input, firstValue);

            return firstValue;
        }

        private long TestStartValue(IEnumerable<string> input, long startingValue)
        {
            var instructions = new InstructionCollection();
            var processor = new Processor(instructions);

            var program = ProgramParser.ParseCommandNameProgram(input);
            var ipRegistry = ParseIpRegistryIndex(input);

            var ip = new InstructionPointer();
            var registry = new Register(new long[] { startingValue, 0, 0, 0, 0, 0 });

            processor.Execute(program,
                                ip,
                                registry,
                                () =>
                                {
                                    registry.SetValue(ipRegistry, ip.Value);
                                },
                                () => ip.Value = registry.GetValue(ipRegistry));

            return startingValue;
        }

        public long Second(IEnumerable<string> input)
        {
            var visited = new HashSet<long>();

            Simulate(r2 =>
            {
                if (visited.Contains(r2))
                {
                    return true;
                }
                visited.Add(r2);
                return false;
            });

            return visited.Last();
        }

        private static void Simulate(Func<long, bool> callback)
        {
            long R0 = 0;
            long R1 = 0;
            long R2 = 0;
            long R3 = 0;
            long R5 = 0;

            var goto8 = false;
            while (true)
            {
                if (!goto8)
                {
                    R5 = R2 | 65536;
                    R2 = 2238642;
                }
                else
                {
                    goto8 = false;
                }

                R3 = R5 & 255;
                R2 += R3;
                R2 = ((R2 & 16777215) * 65899) & 16777215;

                if (256 > R5)
                {
                    if (callback(R2))
                    {
                        break;
                    }

                    if (R0 == R2)
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    R3 = 0;
                    R1 = R3 + 1;
                    R1 *= 256;
                    while (!(R1 > R5))
                    {
                        R3 += 1;

                        R1 = R3 + 1;
                        R1 *= 256;
                    }
                    R5 = R3;
                    goto8 = true;
                }
            }
        }

        private int ParseIpRegistryIndex(IEnumerable<string> input)
        {
            var parts = input.First().Split(' ');

            return int.Parse(parts[1]);
        }
    }
}
