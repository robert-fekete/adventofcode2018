﻿using System.Linq;

namespace adventofcode2018.Day05
{
    internal class Solver
    {
        public int First(string input)
        {
            var polymer = new Polymer(input);
            var reduced = polymer.Reduce();

            return reduced.Length;
        }

        public int Second(string input)
        {
            var units = input.ToLower().Distinct();
            var polymer = new Polymer(input);

            var minLength = input.Length;
            foreach(var unit in units)
            {
                var reduced = polymer.Reduce((s, i) => IgnoreCharacter(s, i, unit));
                var length = reduced.Length;
                if (length < minLength)
                {
                    minLength = length;
                }
            }

            return minLength;
        }

        private static int IgnoreCharacter(string polymer, int currentPos, char toIgnore)
        {
            currentPos++;
            while (currentPos < polymer.Length && char.ToLowerInvariant(polymer[currentPos]) == toIgnore)
            {
                currentPos++;
            }

            return currentPos;
        }
    }
}
