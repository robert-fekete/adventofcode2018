﻿using System;
using System.Collections.Generic;
using System.Text;

namespace adventofcode2018.Day05
{
    internal class Polymer
    {
        private readonly string descriptor;

        public Polymer(string descriptor)
        {
            this.descriptor = descriptor;
        }

        public string Reduce()
        {
            return Reduce((_, i) => i + 1);
        }

        public string Reduce(Func<string, int, int> step)
        {
            var previousUnitIndices = new Stack<int>();

            int iter = step(descriptor, -1);
            while(iter < descriptor.Length)
            {
                if (previousUnitIndices.Count == 0)
                {
                    previousUnitIndices.Push(iter);
                    iter = step(descriptor, iter);
                    continue;
                }
                else
                {
                    var prev = previousUnitIndices.Peek();
                    if (IsReact(prev, iter))
                    {
                        previousUnitIndices.Pop();
                        iter = step(descriptor, iter);
                        continue;
                    }
                    else
                    {
                        previousUnitIndices.Push(iter);
                        iter = step(descriptor, iter);
                        continue;
                    }
                }
            }

            var builder = new StringBuilder();
            foreach(var index in previousUnitIndices)
            {
                builder.Insert(0, descriptor[index]);
            }

            return builder.ToString();
        }

        private bool IsReact(int prev, int iter)
        {
            return descriptor[prev] != descriptor[iter] &&
                (char.ToLowerInvariant(descriptor[prev]) == descriptor[iter] ||
                descriptor[prev] == char.ToLowerInvariant(descriptor[iter]));
        }
    }
}
