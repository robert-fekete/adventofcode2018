﻿namespace adventofcode2018.Day11
{
    internal class CostCalculator
    {
        private readonly int serialNumber;

        public CostCalculator(int serialNumber)
        {
            this.serialNumber = serialNumber;
        }

        public int CalculateCost(int x, int y)
        {
            var rackId = (x + 10);
            long cost = rackId * y;
            cost += serialNumber;
            cost *= rackId;

            int hundredsDigit = (int)((cost % 1000) / 100);

            return hundredsDigit - 5;
        }
    }
}
