﻿using System;

namespace adventofcode2018.Day11
{
    internal class FuelGrid
    {
        private readonly CostCalculator costCalculator;
        private readonly int width;
        private readonly int height;

        public FuelGrid(CostCalculator costCalculator, int width, int height)
        {
            this.costCalculator = costCalculator;
            this.width = width;
            this.height = height;
        }

        // Using this function to find the solution for the second part would result in
        // four nested four ranging [0..w]*[0..h]*w*h (amortized O(n^4), which is ~10^10 
        // magnitude. We can do better than that -> (int, int, int) GetLargestTotalPower()
        public (int, int) GetLargestTotalPower(int targetWidth, int targetHeight)
        {
            var maxPower = 0;
            (int, int) coordinate = (0, 0);
            for (int y = 0; y < height - targetHeight + 1; y++)
            {
                for (int x = 0; x < width - targetHeight + 1; x++)
                {
                    var power = CalculateTargetPower(x, y, targetWidth, targetHeight);
                    if (power > maxPower)
                    {
                        maxPower = power;
                        coordinate = (x, y);
                    }
                }
            }

            return coordinate;
        }

        // Using this function to find a variable size max are costs w*[w..0]*h iterations
        // (amortized O(n^3), which is ~10^7. This is significantly better.
        public (int, int, int) GetLargestTotalPower()
        {
            var maxSubArrayCalculator = new MaximumSubArray2D(width, height, costCalculator.CalculateCost);

            return maxSubArrayCalculator.GetMaxSumSubArray();
        }

        private int CalculateTargetPower(int initialX, int initialY, int targetWidth, int targetHeight)
        {
            var total = 0;
            for (int y = 0; y < targetHeight; y++)
            {
                for (int x = 0; x < targetWidth; x++)
                {
                    total += costCalculator.CalculateCost(x + initialX, y + initialY);
                }
            }

            return total;
        }
    }
}
