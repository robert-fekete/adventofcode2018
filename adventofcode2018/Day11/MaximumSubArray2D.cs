﻿using System;

namespace adventofcode2018.Day11
{
    internal class MaximumSubArray2D
    {
        private readonly int width;
        private readonly int height;
        private readonly Func<int, int, int> getValue;

        public MaximumSubArray2D(int width, int height, Func<int, int, int> getValue)
        {
            this.width = width;
            this.height = height;
            this.getValue = getValue;
        }

        public (int x, int y, int size) GetMaxSumSubArray()
        {
            int x = -1;
            int y = -1;
            int sizeofInterest = -1;
            int maxSum = int.MinValue;
            for(int leftCol = 0; leftCol < width; leftCol++)
            {
                var tempColumn = new int[height];

                for(int rightCol = leftCol; rightCol < width; rightCol++)
                {
                    int size = rightCol - leftCol + 1;

                    AddColumn(tempColumn, rightCol);
                    (var tempY, var sum) = GetMaxSum(tempColumn, size);
                    if (sum > maxSum)
                    {
                        y = tempY;
                        x = leftCol;
                        sizeofInterest = size;
                        maxSum = sum;
                    }
                }
            }

            return (x, y, sizeofInterest);
        }

        private (int y, int sum) GetMaxSum(int[] tempColumn, int size)
        {
            var sum = 0;
            for(int i = 0; i < size; i++)
            {
                sum += tempColumn[i];
            }

            var maxSum = sum;
            var y = 0;
            for (int i = size; i < tempColumn.Length; i++)
            {
                sum -= tempColumn[i - size];
                sum += tempColumn[i];

                if (sum > maxSum)
                {
                    y = i - size + 1;
                    maxSum = sum;
                }
            }

            return (y, maxSum);
        }

        private void AddColumn(int[] tempColumn, int rightCol)
        {
            for (int y = 0; y < height; y++)
            {
                tempColumn[y] += getValue(rightCol, y);
            }
        }
    }
}
