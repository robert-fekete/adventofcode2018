﻿using System;

namespace adventofcode2018.Day11
{
    internal class Solver
    {
        public string First(string input)
        {
            var serialNumber = int.Parse(input);
            var calculator = new CostCalculator(serialNumber);
            var grid = new FuelGrid(calculator, 300, 300);

            (int x, int y) = grid.GetLargestTotalPower(3, 3);

            return $"{x},{y}";
        }

        internal string Second(string input)
        {
            var serialNumber = int.Parse(input);
            var calculator = new CostCalculator(serialNumber);
            var grid = new FuelGrid(calculator, 300, 300);

            (int x, int y, int size) = grid.GetLargestTotalPower();

            return $"{x},{y},{size}";
        }
    }
}
