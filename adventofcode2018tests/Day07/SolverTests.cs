﻿using adventofcode2018.Day07;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day07
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"Step C must be finished before step A can begin.
                        Step C must be finished before step F can begin.
                        Step A must be finished before step B can begin.
                        Step A must be finished before step D can begin.
                        Step B must be finished before step E can begin.
                        Step D must be finished before step E can begin.
                        Step F must be finished before step E can begin.".Split('\n')
                        .Select(line => line.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("CABDFE", result);
        }

        [TestMethod]
        public void TestMultiLevelDependency()
        {
            var input = @"Step C must be finished before step A can begin.
                        Step C must be finished before step H can begin.
                        Step A must be finished before step B can begin.
                        Step A must be finished before step D can begin.
                        Step B must be finished before step E can begin.
                        Step D must be finished before step E can begin.
                        Step H must be finished before step G can begin.
                        Step G must be finished before step F can begin.
                        Step F must be finished before step E can begin.".Split('\n')
                        .Select(line => line.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("CABDHGFE", result);
        }

        [TestMethod]
        public void TestTwoHeadGraph()
        {
            var input = @"Step C must be finished before step A can begin.
                        Step C must be finished before step F can begin.
                        Step A must be finished before step B can begin.
                        Step A must be finished before step D can begin.
                        Step B must be finished before step E can begin.
                        Step D must be finished before step E can begin.
                        Step H must be finished before step G can begin.
                        Step G must be finished before step F can begin.
                        Step F must be finished before step E can begin.".Split('\n')
                        .Select(line => line.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("CABDHGFE", result);
        }

        [TestMethod]
        public void TestAlphabetical()
        {
            var input = @"Step C must be finished before step A can begin.
                        Step C must be finished before step B can begin.
                        Step A must be finished before step D can begin.
                        Step B must be finished before step F can begin.
                        Step F must be finished before step E can begin.
                        Step D must be finished before step E can begin.".Split('\n')
                        .Select(line => line.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("CABDFE", result);
        }

        [TestMethod]
        [DeploymentItem("Day07\\input.txt", "Day07")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day07\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("CQSWKZFJONPBEUMXADLYIGVRHT", result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"Step C must be finished before step A can begin.
                        Step C must be finished before step F can begin.
                        Step A must be finished before step B can begin.
                        Step A must be finished before step D can begin.
                        Step B must be finished before step E can begin.
                        Step D must be finished before step E can begin.
                        Step F must be finished before step E can begin.".Split('\n')
                        .Select(line => line.Trim());

            var solver = new Solver();
            var result = solver.Second(input, 2, 0);

            Assert.AreEqual(15, result);
        }

        [TestMethod]
        [DeploymentItem("Day07\\input.txt", "Day07")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day07\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input, 5, 60);

            Assert.AreEqual(914, result);
        }
    }
}
