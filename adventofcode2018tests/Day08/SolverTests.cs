﻿using adventofcode2018.Day08;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace adventofcode2018tests.Day08
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(138, result);
        }

        [TestMethod]
        [DeploymentItem("Day08\\input.txt", "Day08")]
        public void TestFirstInput()
        {
            var input = File.ReadAllText("Day08\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(48155, result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2";

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(66, result);
        }

        [TestMethod]
        [DeploymentItem("Day08\\input.txt", "Day08")]
        public void TestSecondInput()
        {
            var input = File.ReadAllText("Day08\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(40292, result);
        }
    }
}
