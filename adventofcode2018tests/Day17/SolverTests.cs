﻿using adventofcode2018.Day17;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day17
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"x=495, y=2..7
                        y=7, x=495..501
                        x=501, y=3..7
                        x=498, y=2..4
                        x=506, y=1..2
                        x=498, y=10..13
                        x=504, y=10..13
                        y=13, x=498..504".Split('\n').Select(p => p.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(57, result);
        }

        [TestMethod]
        public void TestStackingOnFlowing()
        {
            var input = @"y=1, x=494
                            y=2, x=498
                            y=2, x=502
                            y=3, x=498..502
                            y=6, x=504
                            y=7, x=496..504
                            y=9, x=494".Split('\n').Select(p => p.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(30, result);
        }

        [TestMethod]
        public void TestBucketInBucket()
        {
            var input = @"y=0, x=491
                            y=0, x=505
                            y=3..10, x=493
                            y=10, x=494..502
                            y=3..10, x=503
                            y=5..7, x=496
                            y=5..7, x=499
                            y=7, x=497..498".Split('\n').Select(p => p.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(86, result);
        }

        [TestMethod]
        [DeploymentItem("Day17\\input.txt", "Day17")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day17\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(34291, result);
        }
        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"x=495, y=2..7
                        y=7, x=495..501
                        x=501, y=3..7
                        x=498, y=2..4
                        x=506, y=1..2
                        x=498, y=10..13
                        x=504, y=10..13
                        y=13, x=498..504".Split('\n').Select(p => p.Trim());

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(29, result);
        }

        [TestMethod]
        [DeploymentItem("Day17\\input.txt", "Day17")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day17\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(28487, result);
        }
    }
}
