﻿using adventofcode2018.Day11;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day11
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class FuelGridTests
    {
        [TestMethod]
        public void Test1()
        {
            var calculator = new CostCalculator(18);
            var grid = new FuelGrid(calculator, 300, 300);
            var result = grid.GetLargestTotalPower(3, 3);

            Assert.AreEqual((33, 45), result);
        }

        [TestMethod]
        public void Test2()
        {
            var calculator = new CostCalculator(42);
            var grid = new FuelGrid(calculator, 300, 300);
            var result = grid.GetLargestTotalPower(3, 3);

            Assert.AreEqual((21, 61), result);
        }
    }
}
