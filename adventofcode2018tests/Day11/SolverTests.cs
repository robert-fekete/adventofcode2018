﻿using adventofcode2018.Day11;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace adventofcode2018tests.Day11
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"18";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("33,45", result);
        }

        [TestMethod]
        public void TestFirstInput()
        {
            var input = "6878";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("20,34", result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"18";

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual("90,269,16", result);
        }

        [TestMethod]
        public void TestSecondExample2()
        {
            var input = @"42";

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual("232,251,12", result);
        }

        [TestMethod]
        public void TestSecondInput()
        {
            var input = "6878";

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual("90,57,15", result);
        }
    }
}
