﻿using adventofcode2018.Day11;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day11
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class CostCalculatorTests
    {
        [TestMethod]
        public void Test1()
        {
            var calculator = new CostCalculator(8);
            var result = calculator.CalculateCost(3, 5);

            Assert.AreEqual(4, result);
        }
        [TestMethod]
        public void Test2()
        {
            var calculator = new CostCalculator(57);
            var result = calculator.CalculateCost(122, 79);

            Assert.AreEqual(-5, result);
        }
        [TestMethod]
        public void Test3()
        {
            var calculator = new CostCalculator(39);
            var result = calculator.CalculateCost(217, 196);

            Assert.AreEqual(0, result);
        }
        [TestMethod]
        public void Test4()
        {
            var calculator = new CostCalculator(71);
            var result = calculator.CalculateCost(101, 153);

            Assert.AreEqual(4, result);
        }
    }
}
