﻿using adventofcode2018.Day24;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day24
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"Immune System:
                          17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
                          989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3
                          
                          Infection:
                          801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
                          4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(5216, result);
        }

        [TestMethod]
        [DeploymentItem("Day24\\input.txt", "Day24")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day24\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(18280, result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"Immune System:
                          17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
                          989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3
                          
                          Infection:
                          801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
                          4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(51, result);
        }

        [TestMethod]
        [DeploymentItem("Day24\\input.txt", "Day24")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day24\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(4573, result);
        }
    }
}
