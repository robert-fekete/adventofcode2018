﻿using adventofcode2018.Day14;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace adventofcode2018tests.Day14
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"2018";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("5941429882", result);
        }

        [TestMethod]
        public void TestFirstInput()
        {
            var input = "793061";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("4138145721", result);
        }

        [TestMethod]
        public void TestSecondExample1()
        {
            var input = @"59414";

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(2018, result);
        }

        [TestMethod]
        public void TestSecondExample2()
        {
            var input = @"51589";

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(9, result);
        }

        [TestMethod]
        public void TestSecondExample3()
        {
            var input = @"01245";

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(5, result);
        }

        [TestMethod]
        public void TestSecondExample4()
        {
            var input = @"92510";

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(18, result);
        }

        [TestMethod]
        public void TestSecondInput()
        {
            var input = "793061";

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(20276284, result);
        }
    }
}
