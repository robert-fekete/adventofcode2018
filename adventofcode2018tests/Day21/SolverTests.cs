﻿using adventofcode2018.Day21;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day21
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        [DeploymentItem("Day21\\input.txt", "Day21")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day21\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(13970209, result);
        }

        [TestMethod]
        [DeploymentItem("Day21\\input.txt", "Day21")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day21\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(6267260, result);
        }
    }
}
