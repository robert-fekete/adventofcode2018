﻿using adventofcode2018.Day23;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day23
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"pos=<0,0,0>, r=4
                          pos=<1,0,0>, r=1
                          pos=<4,0,0>, r=3
                          pos=<0,2,0>, r=1
                          pos=<0,5,0>, r=3
                          pos=<0,0,3>, r=1
                          pos=<1,1,1>, r=1
                          pos=<1,1,2>, r=1
                          pos=<1,3,1>, r=1".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(7, result);
        }

        [TestMethod]
        [DeploymentItem("Day23\\input.txt", "Day23")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day23\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(737, result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"pos=<10,12,12>, r=2
                          pos=<12,14,12>, r=2
                          pos=<16,12,12>, r=4
                          pos=<14,14,14>, r=6
                          pos=<50,50,50>, r=200
                          pos=<10,10,10>, r=5".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(36, result);
        }

        [TestMethod]
        [DeploymentItem("Day23\\input.txt", "Day23")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day23\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(123356173, result);
        }
    }
}
