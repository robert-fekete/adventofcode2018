﻿using adventofcode2018.Day06;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day06
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        //[TestMethod]
        [DeploymentItem("Day06\\input.txt", "Day06")]
        public void GenerateImages()
        {
            var coordinates = File.ReadAllLines("Day06\\input.txt").
                                Select(line => line.Trim()).
                                Select(line => line.Split(',').
                                    Select(p => p.Trim()).
                                    Select(p => int.Parse(p))).
                                Select(parts => (parts.First(), parts.Skip(1).First()));
            var generator = new ImageGenerator(coordinates);
            generator.SaveImage(@"..\..\..\Day06_big.png", 0);
            generator.SaveImage(@"..\..\..\Day06_big1.png", 10);
            generator.SaveImage(@"..\..\..\Day06_big2.png", 150);

            var coordinatesSmall = @"1, 1
                                1, 6
                                8, 3
                                3, 4
                                5, 5
                                8, 9".Split('\n').
                                Select(line => line.Trim()).
                                Select(line => line.Split(',').
                                    Select(p => p.Trim()).
                                    Select(p => int.Parse(p))).
                                Select(parts => (parts.First(), parts.Skip(1).First()));
            var generatorSmall = new ImageGenerator(coordinatesSmall);
            generatorSmall.SaveImage(@"..\..\..\Day06_small.png", 1);
            generatorSmall.SaveImage(@"..\..\..\Day06_small2.png", 10);
        }

        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"1, 1
                        1, 6
                        8, 3
                        3, 4
                        5, 5
                        8, 9".Split('\n')
                        .Select(line => line.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(17, result);
        }

        [TestMethod]
        [DeploymentItem("Day06\\input.txt", "Day06")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day06\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(4398, result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"1, 1
                        1, 6
                        8, 3
                        3, 4
                        5, 5
                        8, 9".Split('\n')
                        .Select(line => line.Trim());

            var solver = new Solver();
            var result = solver.Second(input, 32);

            Assert.AreEqual(16, result);
        }

        [TestMethod]
        [DeploymentItem("Day06\\input.txt", "Day06")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day06\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input, 10000);

            Assert.AreEqual(39560, result);
        }
    }
}
