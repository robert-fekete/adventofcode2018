﻿using adventofcode2018.Day15;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day15
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample1()
        {
            var input = @"#######
                         #.G...#
                         #...EG#
                         #.#.#G#
                         #..G#E#
                         #.....#
                         #######".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input, 1);

            Assert.AreEqual(27730, result);
        }

        [TestMethod]
        public void TestFirstExample2()
        {
            var input = @"#######
                          #G..#E#
                          #E#E.E#
                          #G.##.#
                          #...#E#
                          #...E.#
                          #######".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input, 2);

            Assert.AreEqual(36334, result);
        }

        [TestMethod]
        public void TestFirstExample3()
        {
            var input = @"#########
                          #G......#
                          #.E.#...#
                          #..##..G#
                          #...##..#
                          #...#...#
                          #.G...G.#
                          #.....G.#
                          #########".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input, 3);

            Assert.AreEqual(18740, result);
        }

        [TestMethod]
        [DeploymentItem("Day15\\input.txt", "Day15")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day15\\input.txt");

            var solver = new Solver();
            var result = solver.First(input, 4);

            Assert.AreEqual(188576, result);
        }
        [TestMethod]
        public void TestSecondExample1()
        {
            var input = @"#######
                         #.G...#
                         #...EG#
                         #.#.#G#
                         #..G#E#
                         #.....#
                         #######".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(4988, result);
        }

        [TestMethod]
        public void TestSecondExample2()
        {
            var input = @"#######
                          #E..EG#
                          #.#G.E#
                          #E.##E#
                          #G..#.#
                          #..E#.#
                          #######".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(31284, result);
        }

        [TestMethod]
        public void TestSecondExample3()
        {
            var input = @"#######
                         #E.G#.#
                         #.#G..#
                         #G.#.G#
                         #G..#.#
                         #...E.#
                         #######".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(3478, result);
        }

        [TestMethod]
        public void TestSecondExample4()
        {
            var input = @"#######
                          #.E...#
                          #.#..G#
                          #.###.#
                          #E#G#G#
                          #...#G#
                          #######".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(6474, result);
        }

        [TestMethod]
        public void TestSecondExample5()
        {
            var input = @"#########
                          #G......#
                          #.E.#...#
                          #..##..G#
                          #...##..#
                          #...#...#
                          #.G...G.#
                          #.....G.#
                          #########".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.Second(input, 5);

            Assert.AreEqual(1140, result);
        }

        [TestMethod]
        [DeploymentItem("Day15\\input.txt", "Day15")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day15\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input, 6);

            Assert.AreEqual(57112, result);
        }
    }
}
