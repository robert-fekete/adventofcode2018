﻿using adventofcode2018.Day15;
using adventofcode2018.Day15.Graphs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day15
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void TestUnitLosesHealthOnAttack()
        {
            var unit = new Unit(false, 100, 100, new Node(1, 1));
            var attacker = new Unit(false, 50, 100, new Node(1, 2));

            attacker.Attack(unit);

            Assert.AreEqual(50, unit.HitPoints);
        }

        [TestMethod]
        public void TestUnitDiesAfterDamageEqualsToHitPoints()
        {
            var hp = 100;
            var unit = new Unit(false, 100, hp, new Node(1, 1));
            var attacker = new Unit(false, hp, 100, new Node(1, 2));

            attacker.Attack(unit);

            Assert.IsFalse(unit.IsAlive);
        }

        [TestMethod]
        public void TestStartingNodeIsNotVisitable()
        {
            var node = new Node(1, 1);

            var unit = new Unit(false, 100, 100, node);

            Assert.IsFalse(node.IsVisitable);
        }

        [TestMethod]
        public void TestStepToNodesAreUpdated()
        {
            var node1 = new Node(1, 1);
            var node2 = new Node(1, 2);

            var unit = new Unit(false, 100, 100, node1);
            unit.StepTo(node2);

            Assert.IsTrue(node1.IsVisitable);
            Assert.IsFalse(node2.IsVisitable);
        }

        [TestMethod]
        public void TestUnitFreesNodeOnDying()
        {
            var node = new Node(1, 1);

            var unit = new Unit(false, 100, 100, node);
            var attacker = new Unit(false, 10000000, 100, new Node(1, 2));
            attacker.Attack(unit);

            Assert.IsTrue(node.IsVisitable);
        }
    }
}
