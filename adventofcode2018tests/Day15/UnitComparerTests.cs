﻿using adventofcode2018.Day15;
using adventofcode2018.Day15.Graphs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day15
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class UnitComparerTests
    {
        [TestMethod]
        public void TestLowerHealth()
        {
            var firstUnit = new Unit(false, 100, 100, new Node(1, 1));
            var secondUnit = new Unit(false, 100, 200, new Node(1, 0));

            var units = new List<Unit>() { secondUnit, firstUnit };
            units.Sort(new UnitComparer());

            Assert.AreEqual(firstUnit, units[0]);
        }

        [TestMethod]
        public void TestSameHealth()
        {
            var firstUnit = new Unit(false, 100, 100, new Node(1, 0));
            var secondUnit = new Unit(false, 100, 100, new Node(1, 1));

            var units = new List<Unit>() { secondUnit, firstUnit };
            units.Sort(new UnitComparer());

            Assert.AreEqual(firstUnit, units[0]);
        }
    }
}
