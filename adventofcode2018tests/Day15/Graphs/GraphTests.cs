﻿using adventofcode2018.Day15.Graphs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace adventofcode2018tests.Day15
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class GraphTests
    {
        [TestMethod]
        public void TestFindClosestWhenDifferentDistance()
        {
            var graph = GenerateGraph(3, 4);
            var result = graph.FindClosestOfNodes(new Node(0, 0), new Node[] { new Node(3, 0), new Node(0, 2) });

            Assert.AreEqual(new Node(0, 2), result.GetClosestNode());
        }

        [TestMethod]
        public void TestFindClosestWhenSameDistance()
        {
            var graph = GenerateGraph(3, 4);
            var result = graph.FindClosestOfNodes(new Node(0, 0), new Node[] { new Node(2, 0), new Node(0, 2) });

            Assert.AreEqual(new Node(2, 0), result.GetClosestNode());
        }

        [TestMethod]
        public void TestGetFirstNodeInPath1()
        {
            var graph = GenerateGraph(3, 4);
            var result = graph.FindClosestOfNodes(new Node(0, 0), new Node[] { new Node(2, 0), new Node(0, 2) });

            Assert.AreEqual(new Node(1, 0), result.GetFirstNodeInPathTo(new Node(2, 0)));
        }

        [TestMethod]
        public void TestGetFirstNodeInPath2()
        {
            var graph = GenerateGraph(3, 4);
            var result = graph.FindClosestOfNodes(new Node(0, 0), new Node[] { new Node(2, 0), new Node(0, 2) });

            Assert.AreEqual(new Node(0, 1), result.GetFirstNodeInPathTo(new Node(0, 2)));
        }

        [TestMethod]
        public void TestGetFirstNodeInPathWhenNeighbor()
        {
            var graph = GenerateGraph(3, 4);
            var result = graph.FindClosestOfNodes(new Node(0, 0), new Node[] { new Node(2, 0), new Node(0, 2) });

            Assert.AreEqual(new Node(0, 1), result.GetFirstNodeInPathTo(new Node(0, 1)));
        }

        [TestMethod]
        public void TestGetFirstNodeInPathWhenPathIsBlocked()
        {
            var graph = GenerateGraph(3, 4);
            graph.GetEmptyNodesInRange(new Node(0, 0)).First(n => n.Equals(new Node(0, 1))).StepInto();

            var result = graph.FindClosestOfNodes(new Node(0, 0), new Node[] { new Node(0, 2) });

            Assert.AreEqual(new Node(1, 0), result.GetFirstNodeInPathTo(new Node(0, 2)));
        }

        [TestMethod]
        public void TestGetNodeInRangeOnEdge()
        {
            var graph = GenerateGraph(3, 4);
            var nodes = graph.GetEmptyNodesInRange(new Node(0, 0));

            CollectionAssert.AreEquivalent(new Node[] { new Node(0, 1), new Node(1, 0) }, nodes.ToList());
        }

        [TestMethod]
        public void TestGetNodeInRange()
        {
            var graph = GenerateGraph(3, 4);
            var nodes = graph.GetEmptyNodesInRange(new Node(1, 1));

            CollectionAssert.AreEquivalent(new Node[] { new Node(0, 1), new Node(1, 0), new Node(2, 1), new Node(1, 2) }, nodes.ToList());
        }

        [TestMethod]
        public void TestGetNodeInRangeWhenBlocked()
        {
            var graph = GenerateGraph(3, 4);
            graph.GetEmptyNodesInRange(new Node(1, 1)).First(n => n.Equals(new Node(0, 1))).StepInto();

            var nodes = graph.GetEmptyNodesInRange(new Node(1, 1));

            CollectionAssert.AreEquivalent(new Node[] { new Node(1, 0), new Node(2, 1), new Node(1, 2) }, nodes.ToList());
        }

        private Graph GenerateGraph(int height, int width)
        {

            var nodes = new List<Node>();
            var edges = new Dictionary<Node, List<Node>>();

            for (int x = 0; x < width; x++)
            {
                for(int y = 0; y < height; y++)
                {
                    var node = new Node(x, y);
                    nodes.Add(node);
                    edges[node] = new List<Node>();

                    foreach ((int diffX, int diffY) in new (int, int)[] { (-1, 0), (1, 0), (0, -1), (0, 1)})
                    {
                        var newX = x + diffX;
                        var newY = y + diffY;

                        if (newY >= 0 && newY < height && newX >= 0 && newX < width)
                        {
                            edges[node].Add(new Node(newX, newY));
                        }
                    }
                }
            }

            return new Graph(nodes, edges);
        }
    }
}
