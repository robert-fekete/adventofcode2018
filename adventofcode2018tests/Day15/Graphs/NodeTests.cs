﻿using adventofcode2018.Day15.Graphs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day15
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class NodeTests
    {
        [TestMethod]
        public void TestNodeStopsBeingVisitableIfSteppedInto()
        {
            var node = new Node(1, 0);
            node.StepInto();

            Assert.IsFalse(node.IsVisitable);
        }

        [TestMethod]
        public void TestNodeStartsBeingVisitableIfSteppedOut()
        {
            var node = new Node(1, 0);
            node.StepInto();
            node.StepOut();

            Assert.IsTrue(node.IsVisitable);
        }

        [TestMethod]
        public void TestCompareToNodeBelow()
        {
            var firstNode = new Node(1, 0);
            var secondNode = new Node(1, 1);

            Assert.IsTrue(firstNode.ReadingOrderCompareTo(secondNode) < 0);
        }

        [TestMethod]
        public void TestCompareToNodeLeft()
        {
            var firstNode = new Node(0, 1);
            var secondNode = new Node(1, 1);

            Assert.IsTrue(firstNode.ReadingOrderCompareTo(secondNode) < 0);
        }

        [TestMethod]
        public void TestCompareToNodeRight()
        {
            var firstNode = new Node(2, 1);
            var secondNode = new Node(1, 1);

            Assert.IsTrue(firstNode.ReadingOrderCompareTo(secondNode) > 0);
        }

        [TestMethod]
        public void TestCompareToNodeAbove()
        {
            var firstNode = new Node(1, 2);
            var secondNode = new Node(1, 1);

            Assert.IsTrue(firstNode.ReadingOrderCompareTo(secondNode) > 0);
        }
    }
}
