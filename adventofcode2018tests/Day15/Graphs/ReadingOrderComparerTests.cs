﻿using adventofcode2018.Day15.Graphs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day15
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class ReadingOrderComparerTests
    {
        [TestMethod]
        public void TestOrderingDirection()
        {
            var firstNode = new Node(1, 1);
            var secondNode = new Node(1, 2);

            var nodes = new List<Node>() { secondNode, firstNode };
            nodes.Sort(new ReadingOrderComparer());

            Assert.AreEqual(firstNode, nodes[0]);
        }

        [TestMethod]
        public void TestReadingOrder()
        {
            var firstNode = new Node(1, 0);
            var secondNode = new Node(0, 1);
            var thirdNode = new Node(1, 1);
            var fourthNode = new Node(2, 1);
            var fifthNode = new Node(1, 2);

            var nodes = new List<Node>() { secondNode, firstNode, fifthNode, thirdNode, fourthNode };
            nodes.Sort(new ReadingOrderComparer());

            Assert.AreEqual(firstNode, nodes[0]);
            Assert.AreEqual(secondNode, nodes[1]);
            Assert.AreEqual(thirdNode, nodes[2]);
            Assert.AreEqual(fourthNode, nodes[3]);
            Assert.AreEqual(fifthNode, nodes[4]);
        }
    }
}
