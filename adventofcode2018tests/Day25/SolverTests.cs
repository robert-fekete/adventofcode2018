﻿using adventofcode2018.Day25;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day25
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"0,0,0,0
                          3,0,0,0
                          0,3,0,0
                          0,0,3,0
                          0,0,0,3
                          0,0,0,6
                          9,0,0,0
                         12,0,0,0".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void TestFirstExample1()
        {
            var input = @"-1,2,2,0
                          0,0,2,-2
                          0,0,0,-2
                          -1,2,0,0
                          -2,-2,-2,2
                          3,0,2,-1
                          -1,3,2,2
                          -1,0,-1,0
                          0,2,1,-2
                          3,0,0,0".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(4, result);
        }

        [TestMethod]
        public void TestFirstExample2()
        {
            var input = @"1,-1,0,1
                          2,0,-1,0
                          3,2,-1,0
                          0,0,3,1
                          0,0,-1,-1
                          2,3,-2,0
                          -2,2,0,0
                          2,-2,0,-1
                          1,-1,0,-1
                          3,2,0,2".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(3, result);
        }
        [TestMethod]
        public void TestFirstExample3()
        {
            var input = @"1,-1,-1,-2
                          -2,-2,0,1
                          0,2,1,3
                          -2,3,-2,1
                          0,2,3,-2
                          -1,-1,1,-2
                          0,-2,-1,0
                          -2,2,3,-1
                          1,2,2,0
                          -1,-2,0,-2".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(8, result);
        }
    }
}
