﻿using adventofcode2018.Day02;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day02
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"abcdef
                        bababc
                        abbcde
                        abcccd
                        aabcdd
                        abcdee
                        ababab";
            var solver = new Solver();
            var result = solver.First(input.Split('\n').Select(p => p.Trim()));

            Assert.AreEqual(12, result);
        }

        [TestMethod]
        [DeploymentItem("Day02\\input.txt", "Day02")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day02\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(8398, result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"abcde
                        fghij
                        klmno
                        pqrst
                        fguij
                        axcye
                        wvxyz";
            var solver = new Solver();
            var result = solver.Second(input.Split('\n').Select(p => p.Trim()));

            Assert.AreEqual("fgij", result);
        }

        [TestMethod]
        [DeploymentItem("Day02\\input.txt", "Day02")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day02\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual("hhvsdkatysmiqjxunezgwcdpr", result);
        }
    }
}
