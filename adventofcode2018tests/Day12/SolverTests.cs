﻿using adventofcode2018.Day12;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace adventofcode2018tests.Day12
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        [DeploymentItem("Day12\\example.txt", "Day12")]
        public void TestFirstExample()
        {
            var input = File.ReadAllLines("Day12\\example.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(325, result);
        }

        [TestMethod]
        [DeploymentItem("Day12\\input.txt", "Day12")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day12\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(3903, result);
        }

        [TestMethod]
        [DeploymentItem("Day12\\example.txt", "Day12")]
        public void TestSecondExample()
        {
            var input = File.ReadAllLines("Day12\\example.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(999999999374, result);
        }

        [TestMethod]
        [DeploymentItem("Day12\\input.txt", "Day12")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day12\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(3450000002268, result);
        }
    }
}
