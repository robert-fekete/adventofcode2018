﻿using adventofcode2018.Day12;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day12
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SimulatorTests
    {
        [TestMethod]
        public void TestShiftingGenerations()
        {
            var pots = new PotCollection(new bool[] { true, false, false, false, true, false });
            var patterns = new List<Pattern>()
            {
                new Pattern(new bool[]{false, true, false, false, false}, true)
            };

            var simulator = new Simulator(pots, patterns);
            simulator.SimulateGenerations(2);

            Assert.AreEqual(8, simulator.PlantIndexSum);
        }
    }
}
