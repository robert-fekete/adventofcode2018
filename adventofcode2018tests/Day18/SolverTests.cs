﻿using adventofcode2018.Day18;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day18
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @".#.#...|#.
                        .....#|##|
                        .|..|...#.
                        ..|#.....#
                        #.#|||#|#|
                        ...#.||...
                        .|....|...
                        ||...#|.#|
                        |.||||..|.
                        ...#.|..|.".Split('\n').Select(p => p.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(1147, result);
        }

        [TestMethod]
        [DeploymentItem("Day18\\input.txt", "Day18")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day18\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(480150, result);
        }

        [TestMethod]
        [DeploymentItem("Day18\\input.txt", "Day18")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day18\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(233020, result);
        }
    }
}
