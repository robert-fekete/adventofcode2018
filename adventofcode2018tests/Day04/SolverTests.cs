﻿using adventofcode2018.Day04;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day04
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"[1518-11-01 00:00] Guard #10 begins shift
                        [1518-11-01 00:05] falls asleep
                        [1518-11-01 00:25] wakes up
                        [1518-11-01 00:30] falls asleep
                        [1518-11-01 00:55] wakes up
                        [1518-11-01 23:58] Guard #99 begins shift
                        [1518-11-02 00:40] falls asleep
                        [1518-11-02 00:50] wakes up
                        [1518-11-03 00:05] Guard #10 begins shift
                        [1518-11-03 00:24] falls asleep
                        [1518-11-03 00:29] wakes up
                        [1518-11-04 00:02] Guard #99 begins shift
                        [1518-11-04 00:36] falls asleep
                        [1518-11-04 00:46] wakes up
                        [1518-11-05 00:03] Guard #99 begins shift
                        [1518-11-05 00:45] falls asleep
                        [1518-11-05 00:55] wakes up".Split('\n')
                        .Select(line => line.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(240, result);
        }

        [TestMethod]
        public void TestWithMixedChronology()
        {
            var input = @"[1518-11-04 00:02] Guard #99 begins shift
                        [1518-11-01 00:05] falls asleep
                        [1518-11-01 00:25] wakes up
                        [1518-11-01 00:30] falls asleep
                        [1518-11-01 00:55] wakes up
                        [1518-11-03 00:05] Guard #10 begins shift
                        [1518-11-02 00:40] falls asleep
                        [1518-11-02 00:50] wakes up
                        [1518-11-03 00:24] falls asleep
                        [1518-11-01 23:58] Guard #99 begins shift
                        [1518-11-03 00:29] wakes up
                        [1518-11-01 00:00] Guard #10 begins shift
                        [1518-11-04 00:36] falls asleep
                        [1518-11-04 00:46] wakes up
                        [1518-11-05 00:03] Guard #99 begins shift
                        [1518-11-05 00:45] falls asleep
                        [1518-11-05 00:55] wakes up".Split('\n')
                        .Select(line => line.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(240, result);
        }

        [TestMethod]
        [DeploymentItem("Day04\\input.txt", "Day04")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day04\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(94040, result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"[1518-11-01 00:00] Guard #10 begins shift
                        [1518-11-01 00:05] falls asleep
                        [1518-11-01 00:25] wakes up
                        [1518-11-01 00:30] falls asleep
                        [1518-11-01 00:55] wakes up
                        [1518-11-01 23:58] Guard #99 begins shift
                        [1518-11-02 00:40] falls asleep
                        [1518-11-02 00:50] wakes up
                        [1518-11-03 00:05] Guard #10 begins shift
                        [1518-11-03 00:24] falls asleep
                        [1518-11-03 00:29] wakes up
                        [1518-11-04 00:02] Guard #99 begins shift
                        [1518-11-04 00:36] falls asleep
                        [1518-11-04 00:46] wakes up
                        [1518-11-05 00:03] Guard #99 begins shift
                        [1518-11-05 00:45] falls asleep
                        [1518-11-05 00:55] wakes up".Split('\n')
                        .Select(line => line.Trim());

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(4455, result);
        }

        [TestMethod]
        [DeploymentItem("Day04\\input.txt", "Day04")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day04\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(39940, result);
        }
    }
}
