﻿using adventofcode2018.Day16;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day16
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class InstructionCollectionTests
    {
        [TestMethod]
        public void TestExample()
        {
            var instructions = new InstructionCollection();
            var capture = new Capture(new Register(3, 2, 1, 1), new Register(3, 2, 2, 1), 9, 2, 1, 2);

            var result = instructions.CountInstructionsAboveMatchTreshold(new Capture[]{ capture }, 3);

            Assert.AreEqual(1, result);
        }
    }
}
