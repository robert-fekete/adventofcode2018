﻿using adventofcode2018.Day16;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day16
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"Before: [3, 2, 1, 1]
                        9 2 1 2
                        After:  [3, 2, 2, 1]".Split('\n').Select(p => p.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void TestFirstExampleMultipleCaptures()
        {
            var input = @"Before: [3, 2, 1, 1]
                        9 2 1 2
                        After:  [3, 2, 2, 1]
                        
                        Before: [3, 2, 1, 1]
                        9 2 1 2
                        After:  [3, 2, 2, 1]



                        3 4 5 6
                        1 2 3 4".Split('\n').Select(p => p.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        [DeploymentItem("Day16\\input.txt", "Day16")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day16\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(605, result);
        }

        [TestMethod]
        [DeploymentItem("Day16\\input.txt", "Day16")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day16\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(653, result);
        }
    }
}
