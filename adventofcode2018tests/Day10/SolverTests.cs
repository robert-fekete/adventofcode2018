﻿using adventofcode2018.Day10;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace adventofcode2018tests.Day10
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        [DeploymentItem("Day10\\example.txt", "Day10")]
        public void TestFirstExample()
        {
            var input = File.ReadAllLines("Day10\\example.txt");

            var solver = new Solver();
            var result = solver.First(input, 7);

            result.Save(@"..\..\..\Day10_1.jpg");
        }

        [TestMethod]
        [DeploymentItem("Day10\\input.txt", "Day10")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day10\\input.txt");

            var solver = new Solver();
            var result = solver.First(input, 10);

            result.Save(@"..\..\..\Day10_2.jpg");
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = File.ReadAllLines("Day10\\example.txt");

            var solver = new Solver();
            var result = solver.Second(input, 7);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        [DeploymentItem("Day10\\input.txt", "Day10")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day10\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input, 10);

            Assert.AreEqual(10605, result);
        }
    }
}
