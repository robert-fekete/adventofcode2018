﻿using adventofcode2018.Day03;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day03
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class AreaTests
    {
        [TestMethod]
        public void TestNoOverlap()
        {
            var area1 = new Area(0, 0, 10, 10);
            var area2 = new Area(10, 10, 5, 5);

            Assert.IsFalse(area1.HasOverlap(area2));
        }

        [TestMethod]
        public void TestHasOverlapWithSameArea()
        {
            var area1 = new Area(0, 0, 6, 6);
            var area2 = new Area(0, 0, 6, 6);

            Assert.IsTrue(area1.HasOverlap(area2));
        }

        [TestMethod]
        public void TestHasOverlapWithPerfectSuperSet()
        {
            var area1 = new Area(0, 0, 6, 6);
            var area2 = new Area(2, 2, 2, 2);

            Assert.IsTrue(area2.HasOverlap(area1));
        }

        [TestMethod]
        public void TestHasOverlapWith1DSuperSet()
        {
            var area1 = new Area(0, 0, 1, 3);
            var area2 = new Area(0, 1, 1, 1);

            Assert.IsTrue(area2.HasOverlap(area1));
        }

        [TestMethod]
        public void TestHasOverlapWithEdgedSuperSet()
        {
            var area1 = new Area(0, 0, 6, 6);
            var area2 = new Area(0, 0, 2, 2);

            Assert.IsTrue(area1.HasOverlap(area2));
        }

        [TestMethod]
        public void TestHasOverlapWithBottomEdgedSuperSet()
        {
            var area1 = new Area(0, 0, 6, 6);
            var area2 = new Area(4, 4, 2, 2);

            Assert.IsTrue(area1.HasOverlap(area2));
        }

        [TestMethod]
        public void TestHasOverlapWithNonOverlapCommonEdge()
        {
            var area1 = new Area(0, 0, 1, 3);
            var area2 = new Area(1, 0, 1, 3);

            Assert.IsFalse(area1.HasOverlap(area2));
        }

        [TestMethod]
        public void TestHasOverlapCommutativity()
        {
            for(int x1 = 0; x1 < 4; x1++)
            {
                for(int y1 = 0; y1 < 4; y1++)
                {
                    for(int width1 = 1; width1 < 4; width1++)
                    {
                        for(int height1 = 1; height1 < 4; height1++)
                        {
                            for (int x2 = 0; x2 < 4; x2++)
                            {
                                for (int y2 = 0; y2 < 4; y2++)
                                {
                                    for (int width2 = 1; width2 < 4; width2++)
                                    {
                                        for (int height2 = 1; height2 < 4; height2++)
                                        {
                                            var area1 = new Area(x1, y1, width1, height1);
                                            var area2 = new Area(x2, y2, width2, height2);

                                            var hasOverlap1 = area1.HasOverlap(area2);
                                            var hasOverlap2 = area2.HasOverlap(area1);

                                            Assert.AreEqual(hasOverlap1, hasOverlap2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
