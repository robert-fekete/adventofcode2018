﻿using adventofcode2018.Day03;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day03
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"#1 @ 1,3: 4x4
                        #2 @ 3,1: 4x4
                        #3 @ 5,5: 2x2".Split('\n')
                        .Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Console.WriteLine(Environment.CurrentDirectory);
            Assert.AreEqual(4, result);
        }

        [TestMethod]
        public void TestMultiDigitParsing()
        {
            var input = @"#1 @ 11,13: 10x10
                        #2 @ 13,11: 10x10".Split('\n')
                        .Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Console.WriteLine(Environment.CurrentDirectory);
            Assert.AreEqual(64, result);
        }

        [TestMethod]
        public void TestMultipleOverlap()
        {
            var input = @"#1 @ 1,3: 4x2
                        #2 @ 3,3: 4x2
                        #3 @ 3,5: 2x4".Split('\n')
                        .Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Console.WriteLine(Environment.CurrentDirectory);
            Assert.AreEqual(4, result);
        }

        [TestMethod]
        [DeploymentItem("Day03\\input.txt", "Day03")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day03\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(101565, result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"#1 @ 1,3: 4x4
                        #2 @ 3,1: 4x4
                        #3 @ 5,5: 2x2".Split('\n')
                        .Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.Second(input);

            Console.WriteLine(Environment.CurrentDirectory);
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        [DeploymentItem("Day03\\input.txt", "Day03")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day03\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(656, result);
        }
    }
}
