﻿using adventofcode2018.Day20;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day20
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample1()
        {
            var input = @"^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(23, result);
        }

        [TestMethod]
        public void TestFirstExample2()
        {
            var input = @"^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(31, result);
        }

        [TestMethod]
        public void TestFirstExample3()
        {
            var input = @"^WNE$";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void TestFirstExample4()
        {
            var input = @"^ENWWW(NEEE|SSE(EE|N))$";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(10, result);
        }

        [TestMethod]
        public void TestFirstExample5()
        {
            var input = @"^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(18, result);
        }

        [TestMethod]
        [DeploymentItem("Day20\\input.txt", "Day20")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day20\\input.txt").First();

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(3151, result);
        }

        [TestMethod]
        [DeploymentItem("Day20\\input.txt", "Day20")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day20\\input.txt").First();

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(8784, result);
        }
    }
}
