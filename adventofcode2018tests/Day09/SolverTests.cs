﻿using adventofcode2018.Day09;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace adventofcode2018tests.Day09
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = "9 players; last marble is worth 25 points";

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(32, result);
        }

        [TestMethod]
        [DeploymentItem("Day09\\input.txt", "Day09")]
        public void TestFirstInput()
        {
            var input = File.ReadAllText("Day09\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(436720, result);
        }

        [TestMethod]
        [DeploymentItem("Day09\\input.txt", "Day09")]
        public void TestSecondInput()
        {
            var input = File.ReadAllText("Day09\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(3527845091, result);
        }
    }
}
