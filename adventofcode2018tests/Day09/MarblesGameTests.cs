﻿using adventofcode2018.Day09;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day09
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class MarblesGameTests
    {
        [TestMethod]
        public void Test10Players()
        {
            var game = new MarblesGame(23);
            var result = game.Play(10, 1618);

            Assert.AreEqual(8317, result);
        }

        [TestMethod]
        public void Test13Players()
        {
            var game = new MarblesGame(23);
            var result = game.Play(13, 7999);

            Assert.AreEqual(146373, result);
        }

        [TestMethod]
        public void Test17Players()
        {
            var game = new MarblesGame(23);
            var result = game.Play(17, 1104);

            Assert.AreEqual(2764, result);
        }

        [TestMethod]
        public void Test21Players()
        {
            var game = new MarblesGame(23);
            var result = game.Play(21, 6111);

            Assert.AreEqual(54718, result);
        }

        [TestMethod]
        public void Test30Players()
        {
            var game = new MarblesGame(23);
            var result = game.Play(30, 5807);

            Assert.AreEqual(37305, result);
        }
    }
}
