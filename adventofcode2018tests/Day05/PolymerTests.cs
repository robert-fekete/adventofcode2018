﻿using adventofcode2018.Day05;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace adventofcode2018tests.Day05
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class PolymerTests
    {
        [TestMethod]
        public void TestReduceWithBeginningDisappearing()
        {
            var polymer = new Polymer("daABbCcDefg");
            var result = polymer.Reduce();

            Assert.AreEqual("efg", result);
        }

        [TestMethod]
        public void TestFirstExample()
        {
            var polymer = new Polymer("dabAcCaCBAcCcaDA");
            var result = polymer.Reduce();

            Assert.AreEqual("dabCBAcaDA", result);
        }
    }
}
