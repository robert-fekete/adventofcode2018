﻿using adventofcode2018.Day05;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace adventofcode2018tests.Day05
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var solver = new Solver();
            var result = solver.First("dabAcCaCBAcCcaDA");

            Assert.AreEqual(10, result);
        }

        [TestMethod]
        [DeploymentItem("Day05\\input.txt", "Day05")]
        public void TestFirstInput()
        {
            var input = File.ReadAllText("Day05\\input.txt").Trim();

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(9116, result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var solver = new Solver();
            var result = solver.Second("dabAcCaCBAcCcaDA");

            Assert.AreEqual(4, result);
        }

        [TestMethod]
        [DeploymentItem("Day05\\input.txt", "Day05")]
        public void TestSecondInput()
        {
            var input = File.ReadAllText("Day05\\input.txt").Trim();

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(6890, result);
        }
    }
}
