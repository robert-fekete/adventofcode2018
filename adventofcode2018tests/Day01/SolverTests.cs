﻿using adventofcode2018.Day01;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day01
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = "+1, -2, +3, +1".Split(',').Select(p => p.Trim());
            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        [DeploymentItem("Day01\\input.txt", "Day01")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day01\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(590, result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = "+1, -2, +3, +1".Split(',').Select(p => p.Trim());
            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        [DeploymentItem("Day01\\input.txt", "Day01")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day01\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(83445, result);
        }
    }
}
