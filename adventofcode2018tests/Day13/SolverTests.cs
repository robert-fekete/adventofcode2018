﻿using adventofcode2018.Day13;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day13
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"/->-\        
..........................|   |  /----\
..........................| /-+--+-\  |
..........................| | |  | v  |
..........................\-+-/  \-+--/
..........................  \------/".Split('\n').Select(line => line.Trim('.'));

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("7,3", result);
        }

        [TestMethod]
        [DeploymentItem("Day13\\input.txt", "Day13")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day13\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual("113,136", result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"/>-<\  
..........................|   |  
..........................| /<+-\
..........................| | | v
..........................\>+</ |
..........................  |   ^
..........................  \<->/".Split('\n').Select(line => line.Trim('.'));

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual("6,4", result);
        }

        [TestMethod]
        [DeploymentItem("Day13\\input.txt", "Day13")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day13\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual("114,136", result);
        }
    }
}
