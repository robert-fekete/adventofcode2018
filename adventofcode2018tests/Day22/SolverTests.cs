﻿using adventofcode2018.Day22;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day22
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"depth: 510
                          target: 10,10".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(114, result);
        }

        [TestMethod]
        [DeploymentItem("Day22\\input.txt", "Day22")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day22\\input.txt");

            var solver = new Solver();
            var result = solver.First(input);

            Assert.AreEqual(5637, result);
        }

        [TestMethod]
        public void TestSecondExample()
        {
            var input = @"depth: 510
                          target: 10,10".Split('\n').Select(l => l.Trim());

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(45, result);
        }

        [TestMethod]
        [DeploymentItem("Day22\\input.txt", "Day22")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day22\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(969, result);
        }
    }
}
