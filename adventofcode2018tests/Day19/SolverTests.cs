﻿using adventofcode2018.Day19;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace adventofcode2018tests.Day19
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class SolverTests
    {
        [TestMethod]
        public void TestFirstExample()
        {
            var input = @"#ip 0
                        seti 5 0 1
                        seti 6 0 2
                        addi 0 1 0
                        addr 1 2 3
                        setr 1 0 0
                        seti 8 0 4
                        seti 9 0 5".Split('\n').Select(p => p.Trim());

            var solver = new Solver();
            var result = solver.First(input, 0);

            Assert.AreEqual(6, result);
        }

        [TestMethod]
        [DeploymentItem("Day19\\input.txt", "Day19")]
        public void TestFirstInput()
        {
            var input = File.ReadAllLines("Day19\\input.txt");

            var solver = new Solver();
            var result = solver.First(input, 0);

            Assert.AreEqual(1922, result);
        }

        [TestMethod]
        [DeploymentItem("Day19\\input.txt", "Day19")]
        public void TestSecondInput()
        {
            var input = File.ReadAllLines("Day19\\input.txt");

            var solver = new Solver();
            var result = solver.Second(input);

            Assert.AreEqual(22302144, result);
        }
    }
}
